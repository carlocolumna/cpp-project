from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
import time

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///C:\\Users\\carLo_\\bots\\prototype\\db.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
format = "%d-%m-%Y"

class Student(db.Model):
	__tablename__ = "student"
	id = db.Column('id', db.Integer, primary_key=True)
	discord_id = db.Column('discord_id', db.Integer)
	qualification = db.Column('qualification', db.String(255))
	instructor = db.Column('instructor', db.String(255))
	module = db.Column('module', db.String(255))
	exam = db.Column('exam', db.Integer)
	workshop = db.Column('workshop', db.Integer)
	grade = db.Column('grade', db.String(255))

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/process', methods=['POST'])
def process():
	discordid = request.form['DiscordID']
	qualification = request.form['Qualification']
	instructor = request.form['Instructor']
	module = request.form['Module']
	grade = request.form['Grade']
	exam = request.form['Exam']
	workshop = request.form['Workshop']

	student = Student(discord_id=discordid, qualification=qualification, instructor=instructor, module=module,
					  grade=grade, exam=time.mktime(time.strptime(exam, format)), workshop=time.mktime(time.strptime(workshop, format)))

	db.session.add(student)
	db.session.commit()

	return redirect(url_for('index'))


if __name__ == '__main__':
	app.run(debug=True)