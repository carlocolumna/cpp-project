import discord
import config
from discord.ext import commands


Client = discord.Client()
client = commands.Bot(command_prefix = config.mcbot_prefix)

@client.event
async def on_ready():
	print("Bot is online!");


@client.event
async def on_message(message):
	if message.content.lower().startswith('!grade'):
		userID = message.author.id
		await client.send_message(await client.get_user_info(userID), "<@%s> VP1 Grade: A" % (userID))

	if message.content.lower().startswith('!exam'):
		userID = message.author.id
		await client.send_message(await client.get_user_info(userID), "<@%s> VP1 Exam Date: 14 Jan 2018" % (userID))

	if message.content.lower().startswith('!workshop'):
		userID = message.author.id
		await client.send_message(message.channel, "<@%s> VP1 Workshop Date: 9 Jan 2018" % (userID))

	if message.content.lower().startswith('!qualification'):
		userID = message.author.id
		await client.send_message(message.channel, "<@%s> Advanced Software Development" % (userID))

	if message.content.lower().startswith('!instructor'):
		userID = message.author.id
		await client.send_message(message.channel, "<@%s> Paul Filippi " % (userID))

client.run(config.mcbot_token)

