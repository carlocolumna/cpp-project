import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_ok = model.Wallet()
indiana_ok.wallet_type = False
indiana_ok.address = "PABTUxHbesEn4TNUaUPKvD3qrZTkBqJdnG"
indiana_ok.discord_id = "264272631560142848"
indiana_ok.coin_id = 3

pixel_ok = model.Wallet()
pixel_ok.wallet_type = False
pixel_ok.address = "P9DdwNuSmDJVxC6DYZpp9s1aZLPUxaqhJF"
pixel_ok.discord_id = "157024659546701824"
pixel_ok.coin_id = 3

kali_ok = model.Wallet()
kali_ok.wallet_type = False
kali_ok.address = "PSyJ2Yitnnq1Hiz8nUravAQyPHf3sxW3HN"
kali_ok.discord_id = "273550198691856394"
kali_ok.coin_id = 3

mc_ok = model.Wallet()
mc_ok.wallet_type = True
mc_ok.address = "PWxzuWiahngSkdEV4H381V9Vk2yDYm1Ev7"
mc_ok.discord_id = "388984890076692481"
mc_ok.coin_id = 3

badwolf_ok = model.Wallet()
badwolf_ok.wallet_type = False
badwolf_ok.address = "PT9dhqUR4ZeRkH1gmcuHjuhMALWH5rypvb"
badwolf_ok.discord_id = "311809553626824714"
badwolf_ok.coin_id = 3

babyai_ok = model.Wallet()
babyai_ok.wallet_type = False
babyai_ok.address = "PSh7gZ4xh4tA92wUhMEyoUxHwD11VzXFr6"
babyai_ok.discord_id = "311058941150756864"
babyai_ok.coin_id = 3

riio_ok = model.Wallet()
riio_ok.wallet_type = False
riio_ok.address = "PQoGdhKkpjyi7orT6zuU31qWTyjF4uDUsC"
riio_ok.discord_id = "210913059231760384"
riio_ok.coin_id = 3

session.add(indiana_ok)
session.add(pixel_ok)
session.add(kali_ok)
session.add(mc_ok)
session.add(badwolf_ok)
session.add(babyai_ok)
session.add(riio_ok)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()