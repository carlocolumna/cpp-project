from bitcoinrpc.authproxy import AuthServiceProxy
import model
import time
from datetime import datetime


#force insert of tx that scheduler missed.

engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=False)

# create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

mc_tx = model.Deposit()
mc_tx.wallet_id = 20
mc_tx.txid = "2e9fe6fa1929c7dae0157a253abe995abc71f2ea6aafec9a1777ba000ec7a848"
mc_tx.chksum = mc_tx.txid + "-" + "20" + "forced"
mc_tx.amount_type = True
mc_tx.amount = 8.99998957
mc_tx.received_at = datetime.now()
mc_tx.date = time.mktime(datetime.now().timetuple())
mc_tx.this_month = time.strftime("%m", time.localtime(mc_tx.date))
mc_tx.this_year = time.strftime("%Y", time.localtime(mc_tx.date))
session.add(mc_tx)
session.commit()



