import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_leet = model.Wallet()
indiana_leet.wallet_type = False
indiana_leet.address = "LUtGLdgC8s83v8QNaV6ePhzThSnuohKtsn"
indiana_leet.discord_id = "264272631560142848"
indiana_leet.coin_id = 1

pixel_leet = model.Wallet()
pixel_leet.wallet_type = False
pixel_leet.address = "LPJ3dxV9aX2Yz5RZmZtXQYAsBbhGQrfVTR"
pixel_leet.discord_id = "157024659546701824"
pixel_leet.coin_id = 1

kali_leet = model.Wallet()
kali_leet.wallet_type = False
kali_leet.address = "Li1TXdrzso6zQp1KKH4a8EnhmkucfrqxUV"
kali_leet.discord_id = "273550198691856394"
kali_leet.coin_id = 1

mc_leet = model.Wallet()
mc_leet.wallet_type = True
mc_leet.address = "LhiNfy9aWvC55s7BW6YmqgRZn83iPCFkTZ"
mc_leet.discord_id = "388984890076692481"
mc_leet.coin_id = 1

badwolf_leet = model.Wallet()
badwolf_leet.wallet_type = False
badwolf_leet.address = "LaQ4ERUeB1eKRVRmAKujiuqVbZvb7By11y"
badwolf_leet.discord_id = "311809553626824714"
badwolf_leet.coin_id = 1

babyai_leet = model.Wallet()
babyai_leet.wallet_type = False
babyai_leet.address = "LeMLjzcfVYGDEHRyGKb2syMBeuRtE72J4Y"
babyai_leet.discord_id = "311058941150756864"
babyai_leet.coin_id = 1

riio_leet = model.Wallet()
riio_leet.wallet_type = False
riio_leet.address = "LV19h9KKcsNbCF927A2jEQWZnvqYymqwK2"
riio_leet.discord_id = "210913059231760384"
riio_leet.coin_id = 1

session.add(indiana_leet)
session.add(pixel_leet)
session.add(kali_leet)
session.add(mc_leet)
session.add(badwolf_leet)
session.add(babyai_leet)
session.add(riio_leet)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()