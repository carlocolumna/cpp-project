import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

engine = model.create_engine('mssql+pyodbc://montecrypto:Vz5i?Ey-QS1A@montecrypto', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_nmd = model.Wallet()
indiana_nmd.wallet_type = 1
indiana_nmd.address = "NM77ggDJRHTWZKXuxLz4vwNoDrknnhRf2X"
indiana_nmd.discord_id = "264272631560142848"
indiana_nmd.opening_balance = 0
indiana_nmd.current_balance = 0
indiana_nmd.coin_id = 11

pixel_nmd = model.Wallet()
pixel_nmd.wallet_type = 1
pixel_nmd.address = "NSUeqQydEgLXoS1x421Y8HCoZ3BfVfQ8P1"
pixel_nmd.discord_id = "157024659546701824"
pixel_nmd.opening_balance = 0
pixel_nmd.current_balance = 0
pixel_nmd.coin_id = 11

kali_nmd = model.Wallet()
kali_nmd.wallet_type = 1
kali_nmd.address = "NYTRuTTwks9JE8dBZpZccWKqnnjEKDZtHa"
kali_nmd.discord_id = "273550198691856394"
kali_nmd.opening_balance = 0
kali_nmd.current_balance = 0
kali_nmd.coin_id = 11

mc_nmd = model.Wallet()
mc_nmd.wallet_type = 2
mc_nmd.address = "NihCNoZCzbWZLMHKUAQViacdCxgfy6cGd5"
mc_nmd.discord_id = "388984890076692481"
mc_nmd.opening_balance = 0
mc_nmd.current_balance = 0
mc_nmd.coin_id = 11

badwolf_nmd = model.Wallet()
badwolf_nmd.wallet_type = 1
badwolf_nmd.address = "NdFKHG8Jb7vLzGxMfwMsN5Ry2VSuHa9yQG"
badwolf_nmd.discord_id = "311809553626824714"
badwolf_nmd.opening_balance = 0
badwolf_nmd.current_balance = 0
badwolf_nmd.coin_id = 11

sparkling_nmd = model.Wallet()
sparkling_nmd.wallet_type = 3
sparkling_nmd.address = "NTnR3apDaCS7ikNvvAfDwHbewoRkdxyadE"
sparkling_nmd.discord_id = "311058941150756864"
sparkling_nmd.opening_balance = 0
sparkling_nmd.current_balance = 0
sparkling_nmd.coin_id = 11

riio_nmd = model.Wallet()
riio_nmd.wallet_type = 1
riio_nmd.address = "NcNC7jWq7KokJWt69VLun3eXTjGvkRXHw3"
riio_nmd.discord_id = "210913059231760384"
riio_nmd.opening_balance = 0
riio_nmd.current_balance = 0
riio_nmd.coin_id = 11

mc_stake_nmd = model.Wallet()
mc_stake_nmd.wallet_type = 4
mc_stake_nmd.address = "NSzTou4iWCxG1b5BwgnJFeKjQT64MB4XUR"
#use id of mc-messenger.
mc_stake_nmd.discord_id = "391817760826458115"
mc_stake_nmd.opening_balance = 0
mc_stake_nmd.current_balance = 0
mc_stake_nmd.coin_id = 11

session.add(indiana_nmd)
session.add(pixel_nmd)
session.add(kali_nmd)
session.add(mc_nmd)
session.add(badwolf_nmd)
session.add(sparkling_nmd)
session.add(riio_nmd)
session.add(mc_stake_nmd)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()