import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indianasend = model.Wallet()
indianasend.wallet_type = False
indianasend.address = "Se39yWouHQuaXE2hD1mTEfx1iTGxrtzjjp"
indianasend.discord_id = "264272631560142848"
indianasend.coin_id = 7

pixelsend = model.Wallet()
pixelsend.wallet_type = False
pixelsend.address = "SQAe7NLftADjcNJDVPDpbkRniwvBGCmxhg"
pixelsend.discord_id = "157024659546701824"
pixelsend.coin_id = 7

kalisend = model.Wallet()
kalisend.wallet_type = False
kalisend.address = "SjjmGNgDmCZkp74xFmaP5DYyk5pq52bFri"
kalisend.discord_id = "273550198691856394"
kalisend.coin_id = 7

mcsend = model.Wallet()
mcsend.wallet_type = True
mcsend.address = "SgbgqSPYNMBr9QXzDeVRhZiMwscuSaTC47"
mcsend.discord_id = "388984890076692481"
mcsend.coin_id = 7

badwolfsend = model.Wallet()
badwolfsend.wallet_type = False
badwolfsend.address = "SQLs63udUFw1tS19Ay1scNC5CfymJ1LVjc"
badwolfsend.discord_id = "311809553626824714"
badwolfsend.coin_id = 7

babyaisend = model.Wallet()
babyaisend.wallet_type = False
babyaisend.address = "SZShNyhNxCVLE9YyxTphA8C1dRzfDphgcc"
babyaisend.discord_id = "311058941150756864"
babyaisend.coin_id = 7

riiosend = model.Wallet()
riiosend.wallet_type = False
riiosend.address = "SWsrut7EzxAUs857u1rERwdCri43kZsvSv"
riiosend.discord_id = "210913059231760384"
riiosend.coin_id = 7

session.add(indianasend)
session.add(pixelsend)
session.add(kalisend)
session.add(mcsend)
session.add(badwolfsend)
session.add(babyaisend)
session.add(riiosend)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()