import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_bitb = model.Wallet()
indiana_bitb.wallet_type = False
indiana_bitb.address = "2Jr71nKCSouJ2tFnNN3cECJqZcgD8FUnxo"
indiana_bitb.discord_id = "264272631560142848"
indiana_bitb.coin_id = 4

pixel_bitb = model.Wallet()
pixel_bitb.wallet_type = False
pixel_bitb.address = "2YByuDgf76utZiXjM4QXvQTh7SbdG6FsTm"
pixel_bitb.discord_id = "157024659546701824"
pixel_bitb.coin_id = 4

kali_bitb = model.Wallet()
kali_bitb.wallet_type = False
kali_bitb.address = "2LwUebstorsHiQMaWxdKfFcnTeu9SjkCCi"
kali_bitb.discord_id = "273550198691856394"
kali_bitb.coin_id = 4

mc_bitb = model.Wallet()
mc_bitb.wallet_type = True
mc_bitb.address = "2Ff2vYvuZyAG2HTNucRiYchAG2xcPqDqZH"
mc_bitb.discord_id = "388984890076692481"
mc_bitb.coin_id = 4

badwolf_bitb = model.Wallet()
badwolf_bitb.wallet_type = False
badwolf_bitb.address = "2R2oby4sUZ11J1phVre7szggoYqANMSKR4"
badwolf_bitb.discord_id = "311809553626824714"
badwolf_bitb.coin_id = 4

babyai_bitb = model.Wallet()
babyai_bitb.wallet_type = False
babyai_bitb.address = "2ag5WRFmGzdNpTkhKc8SSpbSLuwwQhTn4e"
babyai_bitb.discord_id = "311058941150756864"
babyai_bitb.coin_id = 4

riio_bitb = model.Wallet()
riio_bitb.wallet_type = False
riio_bitb.address = "2EobewYDXqLr4dWHdRLYhLCWpN8WFEADFP"
riio_bitb.discord_id = "210913059231760384"
riio_bitb.coin_id = 4

session.add(indiana_bitb)
session.add(pixel_bitb)
session.add(kali_bitb)
session.add(mc_bitb)
session.add(badwolf_bitb)
session.add(babyai_bitb)
session.add(riio_bitb)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()