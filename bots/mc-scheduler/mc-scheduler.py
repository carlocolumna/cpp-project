import model
import logging
from bitcoinrpc.authproxy import AuthServiceProxy
import time
from datetime import datetime

logging.basicConfig(filename='errors.log', level=logging.ERROR)

engine = create_engine('mssql+pyodbc://montecrypto:Vz5i?Ey-QS1A@montecrypto', echo=True)
# model.Base.metadata.create_all(bind=engine)

# create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()


def settings():
    user = 'JaWs'
    password = 'pf114834'
    conn = []
    account_addresses = []
    coins = session.query(model.Coin).filter(model.Coin.online == True).all()
    addresses = session.query(model.Wallet).all()

    for coin in coins:
        for address in addresses:
            if coin.id == address.coin_id:
                account_addresses.append(
                    {
                        "wallet_id": address.id,
                        "discord_id": address.discord_id,
                        "wallet_address": address.address,
                        "wallet_type": address.wallet_type
                    }
                )
        thiscoin = coin.__dict__
        thiscoin["addresses"] = account_addresses
        thiscoin["rpc"] = AuthServiceProxy('http://{0}:{1}@127.0.0.1:{2}'.format(user, password, coin.rpc_port))
        thiscoin["user"] = user
        thiscoin["password"] = password
        conn.append(coin.__dict__)
        account_addresses = []
    return conn


data = settings()
global_counter = 0
insert_counter = 0

print("\nProgram is running. Started at {}\n".format(datetime.now()))

while True:
    # setting current time
    now = datetime.now()
    # start header display
    print("=============================")
    print("Connecting to {} Blockchain".format(data[global_counter]["symbol"]))
    print("=============================")
    print("")
    print("Starting tick... @ {}".format(now))

    list_tx = data[global_counter]["rpc"].listtransactions("*", 25, 0)

    for tx in list_tx:
        for wallet in data[global_counter]["addresses"]:
            if wallet["wallet_address"] == tx.get("address", "n/a"):
                chksum = tx["txid"] + "-" + str(wallet["wallet_id"])
                result_chk = session.query(model.Deposit).filter(model.Deposit.chksum == chksum).first()

                if not result_chk:
                    if tx["category"] == 'generate' or tx["category"] == 'stake' or tx["category"] == 'receive':
                        mc_tx = model.Deposit()
                        mc_tx.wallet_id = wallet["wallet_id"]
                        mc_tx.txid = tx["txid"]
                        mc_tx.chksum = tx["txid"] + "-" + str(wallet["wallet_id"])
                        if tx["category"] == 'generate' or tx["category"] == 'stake':
                            mc_tx.amount_type = True
                        else:
                            mc_tx.amount_type = False
                        mc_tx.amount = tx["amount"]
                        mc_tx.received_at = datetime.now()
                        mc_tx.date = tx["time"]
                        mc_tx.this_month = time.strftime("%m", time.localtime(tx["time"]))
                        mc_tx.this_year = time.strftime("%Y", time.localtime(tx["time"]))
                        session.add(mc_tx)
                        session.commit()
                        insert_counter += 1
                        print("inserting {} {} from {}".format(tx["amount"], data[global_counter]["symbol"],
                                                               tx["account"]))
                    else:
                        print("skipping")
                    break
                else:
                    if data[global_counter]["symbol"] == 'SEND':
                        if tx["category"] == 'send' and int(tx['confirmations']) > 0 and int(
                                tx['bcconfirmations']) > 0 and float(tx["amount"]) < 0 and result_chk.amount_type == False:
                            result_chk.amount = 6.79998960
                            result_chk.amount_type = True
                            session.commit()
                            insert_counter += 1
                            print("updating...{} record".format(insert_counter))
                        else:
                            pass
                    else:
                        pass
            else:
                continue

    if global_counter >= len(data) - 1:
        global_counter = 0
    else:
        global_counter += 1

    print("")
    print("Ending tick...{} transaction(s) added to database".format(insert_counter))
    print("")
    insert_counter = 0

    time.sleep(5)
