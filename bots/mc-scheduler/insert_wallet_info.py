import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

engine = model.create_engine('mssql+pyodbc://montecrypto:Vz5i?Ey-QS1A@montecrypto', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_info = model.Wallet()
indiana_info.wallet_type = 1
indiana_info.address = "iNXnBzbeZRXfFuAiWSKu7KrNJpaV9QqxCw"
indiana_info.discord_id = "264272631560142848"
indiana_info.opening_balance = 0
indiana_info.current_balance = 0
indiana_info.coin_id = 6

pixel_info = model.Wallet()
pixel_info.wallet_type = 1
pixel_info.address = "i3b9Y5fzSSZpqGRuoxa9uj94oXwdUQih77"
pixel_info.discord_id = "157024659546701824"
pixel_info.opening_balance = 0
pixel_info.current_balance = 0
pixel_info.coin_id = 6

kali_info = model.Wallet()
kali_info.wallet_type = 1
kali_info.address = "i56YY1rLzbnv1B2sK3iEtx33PM1K8iPPUy"
kali_info.discord_id = "273550198691856394"
kali_info.opening_balance = 0
kali_info.current_balance = 0
kali_info.coin_id = 6

mc_info = model.Wallet()
mc_info.wallet_type = 2
mc_info.address = "iDyMw7KdXHYJUYWDV3a7EkT4uqvYofQRSo"
mc_info.discord_id = "388984890076692481"
mc_info.opening_balance = 0
mc_info.current_balance = 0
mc_info.coin_id = 6

badwolf_info = model.Wallet()
badwolf_info.wallet_type = 1
badwolf_info.address = "iNJvX4UVqey6WLBAPpLosdjdkeJ1Fc9HNS"
badwolf_info.discord_id = "311809553626824714"
badwolf_info.opening_balance = 0
badwolf_info.current_balance = 0
badwolf_info.coin_id = 6

sparkling_info = model.Wallet()
sparkling_info.wallet_type = 3
sparkling_info.address = "iJT4SfJj32SNmeHt5VRPtX4LApbnNMRjvX"
sparkling_info.discord_id = "311058941150756864"
sparkling_info.opening_balance = 0
sparkling_info.current_balance = 0
sparkling_info.coin_id = 6

riio_info = model.Wallet()
riio_info.wallet_type = 1
riio_info.address = "iCg2WuyE96szXZKnB4Sf9JQR3eziGmAUet"
riio_info.discord_id = "210913059231760384"
riio_info.opening_balance = 0
riio_info.current_balance = 0
riio_info.coin_id = 6

mc_stake_info = model.Wallet()
mc_stake_info.wallet_type = 4
mc_stake_info.address = "i9NRqQjfVBgz6MbweTPp5cd4JuLTPwUJvy"
#use id of mc-messenger.
mc_stake_info.discord_id = "391817760826458115"
mc_stake_info.opening_balance = 0
mc_stake_info.current_balance = 0
mc_stake_info.coin_id = 6

session.add(indiana_info)
session.add(pixel_info)
session.add(kali_info)
session.add(mc_info)
session.add(badwolf_info)
session.add(sparkling_info)
session.add(riio_info)
session.add(mc_stake_info)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()