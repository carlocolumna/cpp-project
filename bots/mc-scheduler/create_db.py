import model
import logging
from bitcoinrpc.authproxy import AuthServiceProxy
import time
from datetime import datetime

logging.basicConfig(filename='errors.log', level=logging.ERROR)

engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=False)
model.Base.metadata.create_all(bind=engine)