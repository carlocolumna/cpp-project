import simplejson
import time
from datetime import datetime


def get_last_transactions(model, session, now):
    last_entries = session.query(model.Deposit).filter(model.Deposit.received_at >= now).order_by(model.Deposit.date.desc()).limit(12)

    results = []
    if last_entries:
        for item in last_entries:
            results.append(
                {
                    "Date": "{0:%d-%b-%y} @ {0:%H:%M}".format(datetime.strptime(item.received_at,'%Y-%m-%d %H:%M:%S.%f')),
                    "Member": item.this_wallet.discord_id,
                    "Amount": item.amount,
                    "Coin": item.this_wallet.this_coin.symbol,
                    "Tier": item.this_wallet.this_coin.tier,
                    "Type": item.amount_type
                }
            )

    return results
