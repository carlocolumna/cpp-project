import discord
from discord.ext import commands
import config as cfg
import asyncio
from datetime import datetime, timedelta
import time
import logging
import model
import api

logging.basicConfig(filename='errors.log', level=logging.INFO)
mc_messenger = commands.Bot(command_prefix=cfg.mcbot_prefix)

# DB hooks
engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=False)
session_maker = model.sessionmaker(bind=engine)
session = session_maker()

num_to_word = {
    # '1' : ':one:',
    # '2' : ':two:',
    # '3' : ':three:',
    "4" : "<:four:396325942890463234>",
    "5" : "<:five:396325777550999573>",
    "6" : "<:six:396323965074800640>",
    "T" : "<:regional_indicator_t:396324041302081537>"
 }

@mc_messenger.event
@asyncio.coroutine
def on_ready():
    print('---------')
    print('Logging in as')
    print(mc_messenger.user.name)
    print(mc_messenger.user.id)
    print('---------')


@asyncio.coroutine
def get_latest_tx():
    yield from mc_messenger.wait_until_ready()
    print("Starting monitoring")
    channel_core = discord.Object(id=cfg.mcbot_core_channel_id)
    channel_riio = discord.Object(id=cfg.mc_bot_core_riio_id)

    while not mc_messenger.is_closed:
        print("Starting reporting")
        delta_before = datetime.now()
        delta_now = delta_before - timedelta(hours=1)
        last_check = delta_now
        detla_str = "{} - {} report".format(delta_now.strftime('%H:%M:%S'), delta_before.strftime('%H:%M:%S'))
        results = api.get_last_transactions(model, session, last_check)
        if results:
            embed = discord.Embed(title="{}".format("Latest deposits | stakes alert"), description=detla_str, color=0xff8040)
            embed.set_footer(text="Delivered By " + mc_messenger.user.name + " the " + str(delta_before))
            tx = ""
            tier = {}
            for i in results:
                emoji_type = u'\U0001F4C0' if i["Type"] else u'\U0001F4BF'
                if not tier.get(i["Tier"], ""):
                    tier[i["Tier"]] ="`" + "{0:} : {1: <13} {2: 15,.3f} {3: <6} | T{4:} | {5: >15}".format(emoji_type, cfg.mcbot_members[str(i["Member"])], i["Amount"], i["Coin"], i["Tier"], i["Date"] ) + "`" + "\n"
                else:
                    tier[i["Tier"]] = tier[i["Tier"]] + "`" + "{0:} : {1: <13} {2: 15,.3f} {3: <6} | T{4:} | {5: >15}".format(emoji_type, cfg.mcbot_members[str(i["Member"])], i["Amount"], i["Coin"], i["Tier"], i["Date"] ) + "`" + "\n"

            for level in sorted(tier):
                embed.add_field(name="Tier {}".format(level), value=tier[level], inline=False)

                #embed.add_field(name="\u200b", value=tx, inline=False)
            yield from mc_messenger.send_message(channel_core, embed=embed)
            yield from mc_messenger.send_message(channel_riio, embed=embed)
        else:
            print("nothing new")
        print("Last signal : {} @ {}".format(last_check, str(datetime.now())))
        yield from asyncio.sleep(3600)

mc_messenger.loop.create_task(get_latest_tx())
mc_messenger.run(cfg.mcbot_token)
