import simplejson
import requests
import time
from datetime import datetime
import calendar

def get_status():
    results = {}
    status = False
    minerinfo = requests.get(
        'https://api.nanopool.org/v1/xmr/user/463tWEBn5XZJSxLU6uLQnQ2iY9xuNcDbjLSjkn3XAXHCbLrTTErJrBWYgHJQyrCwkNgYvyV3z8zctJLPCZy24jvb3NiTcTJ.b6922b4ba03e41e9b77677ef2bc69f18fb8cbda4b83a40a397cabf4b6622d3ae')
    jsonminerinfo = simplejson.loads(minerinfo.text)
    if not jsonminerinfo["status"]:
        return simplejson.dumps({'Error': 'Can not connect to API'})

    now = time.mktime(datetime.now().timetuple())
    for pc in jsonminerinfo["data"]["workers"]:
        lastshare = round((now - pc["lastshare"]) / 60)
        if lastshare > 60 and (pc["id"].split("-"))[1] != 'LAP':
            results[pc["id"]] = " last online " + str(round(lastshare / 60)) + " Hour(s) ago"
            status = True

    if not status:
        results["Status"] = "All miners online"

    return results
