from sqlalchemy import create_engine, Column, Integer, String, Boolean, Float, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

Base = declarative_base()
 
class Coin(Base):
    __tablename__ = "coin"
    id = Column('id', Integer, primary_key=True)
    symbol = Column('symbol', String(10), unique=True)
    name = Column('name', String(25), unique=True)
    rpc_port = Column('rpc_port', Integer, unique=True)
    version = Column('version', String(50))
    tier = Column('tier', Integer)
    status = Column ('status', String(25))
    online = Column('online', Boolean, default = False)
    wallets = relationship('Wallet' , backref='this_coin', lazy='dynamic')

class Wallet(Base):
    __tablename__ = "wallet"
    id = Column('id', Integer, primary_key=True)
    wallet_type = Column('wallet_type', Boolean, default=False)
    address = Column('address', String(255), unique=True)
    discord_id = Column('discord_id', Integer, unique=False)
    coin_id = Column(Integer, ForeignKey('coin.id'))
    deposits = relationship('Deposit', backref='this_wallet', lazy='dynamic')

class Deposit(Base):
    __tablename__ = "deposit"
    id = Column ('id', Integer, primary_key=True)
    amount = Column('amount', Float)
    amount_type = Column('amount_type', Boolean, default=False)
    txid = Column('txid', String(255))
    chksum = Column('chksum', String(255), unique=True)
    date =  Column('date', Integer)
    this_month = Column('this_month',Integer)
    this_year = Column('this_year', Integer)
    received_at = Column('received_at', Integer)
    wallet_id = Column(Integer, ForeignKey('wallet.id'))

class Payment(Base):
    __tablename__ = "payment"
    id = Column ('id', Integer, primary_key=True)
    date =  Column('date', Integer)
    coin = Column('name', String(25))
    amount = Column('amount', Float)
    payment_type = Column('payment_type', Boolean, default=False)    #False-payments from staking, True-payments from Mining
    address = Column('address', String(255))
    discord_id = Column('discord_id', Integer)
    this_month = Column('this_month',Integer)
    this_year = Column('this_year', Integer)
    txid = Column('txid', String(255))
 
 
engine = create_engine('sqlite:///C:\\Users\\carLo_\\bots\\db\\db.db', echo=False)
 
# Construct a sessionmaker object
session = sessionmaker()
 
# Bind the sessionmaker to engine
session.configure(bind=engine)
 
# Create all the tables in the database which are
# defined by Base's subclasses such as User
Base.metadata.create_all(engine)

s = session()

def popdb():
	###December 2017
    decMinJaws = Payment(coin="DOGE", date="1516233600", amount=767, payment_type="1", address="AAFwzhTtsu3ouxNrqJdatUwST1hG6VDkds",
        discord_id="264272631560142848", this_month="12", this_year="2017", txid="")
    decMinWolf = Payment(coin="DOGE", date="1516233600", amount=767, payment_type="1", address="A7dMpeqdeAKr3jpmFofjzxT91yZ14kc6xu",
        discord_id="311809553626824714", this_month="12", this_year="2017", txid="")
    decMinPix = Payment(coin="DOGE", date="1516233600", amount=767, payment_type="1", address="A6SyDpvoLuifiegmcFi4mjbV3yCyhJ7q8D",
        discord_id="157024659546701824", this_month="12", this_year="2017", txid="")
    decMinKal = Payment(coin="DOGE", date="1516233600", amount=767, payment_type="1", address="A8egt413qHHi5K6frEwqPxX1DPPouKQNyP",
        discord_id="273550198691856394", this_month="12", this_year="2017", txid="")
    decMinRio = Payment(coin="DOGE", date="1516233600", amount=767, payment_type="1", address="9rzMvprbcf9927TeYQGA9tpb6Y3ZvsVcLo",
        discord_id="210913059231760384", this_month="12", this_year="2017", txid="")
    decMinBaby = Payment(coin="DOGE", date="1516233600", amount=0, payment_type="1", address="A1TsJ9Ynco3P9NbWKgnrBbde5uVMsURwv7",
        discord_id="311058941150756864", this_month="12", this_year="2017", txid="")

    s.add(decMinJaws)
    s.add(decMinWolf)
    s.add(decMinPix)
    s.add(decMinKal)
    s.add(decMinRio)
    s.add(decMinBaby)

    decStJaws = Payment(coin="DOGE", date="1515024000", amount=294.809,  address="AAFwzhTtsu3ouxNrqJdatUwST1hG6VDkds",
        discord_id="264272631560142848", this_month="12", this_year="2017", txid="")
    decStWolf = Payment(coin="DOGE", date="1515024000", amount=294.809,  address="A7dMpeqdeAKr3jpmFofjzxT91yZ14kc6xu",
        discord_id="311809553626824714", this_month="12", this_year="2017", txid="")
    decStPix = Payment(coin="DOGE", date="1515024000", amount=294.809,  address="A6SyDpvoLuifiegmcFi4mjbV3yCyhJ7q8D",
        discord_id="157024659546701824", this_month="12", this_year="2017", txid="")
    decStKal = Payment(coin="DOGE", date="1515024000", amount=294.809,  address="A8egt413qHHi5K6frEwqPxX1DPPouKQNyP",
        discord_id="273550198691856394", this_month="12", this_year="2017", txid="")
    decStRio = Payment(coin="DOGE", date="1515024000", amount=294.809,  address="9rzMvprbcf9927TeYQGA9tpb6Y3ZvsVcLo",
        discord_id="210913059231760384", this_month="12", this_year="2017", txid="")
    decStBaby = Payment(coin="DOGE", date="1515024000", amount=294.809,  address="A1TsJ9Ynco3P9NbWKgnrBbde5uVMsURwv7",
        discord_id="311058941150756864", this_month="12", this_year="2017", txid="")
    decStMc1 = Payment(coin="RDD", date="1515024000", amount=246.27,  address="RktnqF7tw8fggwfx6p44b1VNjMgB14sFtJ",
        discord_id="388984890076692481", this_month="12", this_year="2017", txid="")
    decStMc2 = Payment(coin="BITB", date="1515024000", amount=176.077,  address="2Ff2vYvuZyAG2HTNucRiYchAG2xcPqDqZH",
        discord_id="388984890076692481", this_month="12", this_year="2017", txid="")
    decStMc3 = Payment(coin="BITB", date="1515024000", amount=133.88,  address="2Ff2vYvuZyAG2HTNucRiYchAG2xcPqDqZH",
        discord_id="388984890076692481", this_month="12", this_year="2017", txid="")

    s.add(decStJaws)
    s.add(decStWolf)
    s.add(decStPix)
    s.add(decStKal)
    s.add(decStRio)
    s.add(decStBaby)
    s.add(decStMc1)
    s.add(decStMc2)
    s.add(decStMc3)

    ###November 2017

    novMinJaws = Payment(coin="DOGE", date="1512864000", amount=1853.2029025, payment_type="1", address="AAFwzhTtsu3ouxNrqJdatUwST1hG6VDkds",
        discord_id="264272631560142848", this_month="11", this_year="2017", txid="")
    novMinWolf = Payment(coin="DOGE", date="1512864000", amount=1853.2029025, payment_type="1", address="A7dMpeqdeAKr3jpmFofjzxT91yZ14kc6xu",
        discord_id="311809553626824714", this_month="11", this_year="2017", txid="")
    novMinPix = Payment(coin="DOGE", date="1512864000", amount=1853.2029025, payment_type="1", address="A6SyDpvoLuifiegmcFi4mjbV3yCyhJ7q8D",
        discord_id="157024659546701824", this_month="11", this_year="2017", txid="")
    novMinKal = Payment(coin="DOGE", date="1512864000", amount=1853.2029025, payment_type="1", address="A8egt413qHHi5K6frEwqPxX1DPPouKQNyP",
        discord_id="273550198691856394", this_month="11", this_year="2017", txid="")
    novMinRio = Payment(coin="DOGE", date="1512864000", amount=1853.2029025, payment_type="1", address="9rzMvprbcf9927TeYQGA9tpb6Y3ZvsVcLo",
        discord_id="210913059231760384", this_month="11", this_year="2017", txid="")
    novMinBaby = Payment(coin="DOGE", date="1512864000", amount=0, payment_type="1", address="A1TsJ9Ynco3P9NbWKgnrBbde5uVMsURwv7",
        discord_id="311058941150756864", this_month="11", this_year="2017", txid="")
    novMinMc1 = Payment(coin="RDD", date="1512864000", amount=7398.01, payment_type="1", address="RktnqF7tw8fggwfx6p44b1VNjMgB14sFtJ",
        discord_id="388984890076692481", this_month="11", this_year="2017", txid="")
    novMinMc2 = Payment(coin="BITB", date="1512864000", amount=2589.30, payment_type="1", address="2Ff2vYvuZyAG2HTNucRiYchAG2xcPqDqZH",
        discord_id="388984890076692481", this_month="11", this_year="2017", txid="")
    novMinMc3 = Payment(coin="SEND", date="1512864000", amount=55.48511684, payment_type="1", address="SgbgqSPYNMBr9QXzDeVRhZiMwscuSaTC47",
        discord_id="388984890076692481", this_month="11", this_year="2017", txid="")
    novMinMc4 = Payment(coin="OK", date="1512864000", amount=10.73905487, payment_type="1", address="PWxzuWiahngSkdEV4H381V9Vk2yDYm1Ev7",
        discord_id="388984890076692481", this_month="11", this_year="2017", txid="")

    s.add(novMinJaws)
    s.add(novMinWolf)
    s.add(novMinPix)
    s.add(novMinKal)
    s.add(novMinRio)
    s.add(novMinBaby)
    s.add(novMinMc1)
    s.add(novMinMc2)
    s.add(novMinMc3)
    s.add(novMinMc4)

    novStJaws = Payment(coin="DOGE", date="1512086400", amount=127,  address="AAFwzhTtsu3ouxNrqJdatUwST1hG6VDkds",
        discord_id="264272631560142848", this_month="11", this_year="2017", txid="")
    novStWolf = Payment(coin="DOGE", date="1512086400", amount=113.44,  address="A7dMpeqdeAKr3jpmFofjzxT91yZ14kc6xu",
        discord_id="311809553626824714", this_month="11", this_year="2017", txid="")
    novStPix = Payment(coin="DOGE", date="1512086400", amount=65.26,  address="A6SyDpvoLuifiegmcFi4mjbV3yCyhJ7q8D",
        discord_id="157024659546701824", this_month="11", this_year="2017", txid="")
    novStKal = Payment(coin="DOGE", date="1512086400", amount=126.16,  address="A8egt413qHHi5K6frEwqPxX1DPPouKQNyP",
        discord_id="273550198691856394", this_month="11", this_year="2017", txid="")
    novStRio = Payment(coin="DOGE", date="1512086400", amount=204.98,  address="9rzMvprbcf9927TeYQGA9tpb6Y3ZvsVcLo",
        discord_id="210913059231760384", this_month="11", this_year="2017", txid="")
    novStBaby = Payment(coin="DOGE", date="1512086400", amount=83.33,  address="A1TsJ9Ynco3P9NbWKgnrBbde5uVMsURwv7",
        discord_id="311058941150756864", this_month="11", this_year="2017", txid="")
    novStMc1 = Payment(coin="RDD", date="1512086400", amount=272,  address="RktnqF7tw8fggwfx6p44b1VNjMgB14sFtJ",
        discord_id="388984890076692481", this_month="11", this_year="2017", txid="")
    novStMc2 = Payment(coin="BITB", date="1512086400", amount=80.22,  address="2Ff2vYvuZyAG2HTNucRiYchAG2xcPqDqZH",
        discord_id="388984890076692481", this_month="11", this_year="2017", txid="")
    novStMc3 = Payment(coin="OK", date="1512086400", amount=2.427,  address="PWxzuWiahngSkdEV4H381V9Vk2yDYm1Ev7",
        discord_id="388984890076692481", this_month="11", this_year="2017", txid="")



    s.add(novStJaws)
    s.add(novStWolf)
    s.add(novStPix)
    s.add(novStKal)
    s.add(novStRio)
    s.add(novStBaby)
    s.add(novStMc1)
    s.add(novStMc2)
    s.add(novStMc3)


    ###October 2017

    octMinJaws = Payment(coin="DOGE", date="1510185600", amount=2195.4782125, payment_type="1", address="AAFwzhTtsu3ouxNrqJdatUwST1hG6VDkds",
        discord_id="264272631560142848", this_month="10", this_year="2017", txid="")
    octMinWolf = Payment(coin="DOGE", date="1510185600", amount=2195.4782125, payment_type="1", address="A7dMpeqdeAKr3jpmFofjzxT91yZ14kc6xu",
        discord_id="311809553626824714", this_month="10", this_year="2017", txid="")
    octMinKal = Payment(coin="DOGE", date="1510185600", amount=2195.4782125, payment_type="1", address="A8egt413qHHi5K6frEwqPxX1DPPouKQNyP",
        discord_id="273550198691856394", this_month="10", this_year="2017", txid="")
    octMinRio = Payment(coin="DOGE", date="1510185600", amount=2195.4782125, payment_type="1", address="9rzMvprbcf9927TeYQGA9tpb6Y3ZvsVcLo",
        discord_id="210913059231760384", this_month="10", this_year="2017", txid="")
    octMinBaby = Payment(coin="DOGE", date="1510185600", amount=2195.4782125, payment_type="1", address="A1TsJ9Ynco3P9NbWKgnrBbde5uVMsURwv7",
        discord_id="311058941150756864", this_month="10", this_year="2017", txid="")
    octMinMc1 = Payment(coin="RDD", date="1510185600", amount=7967.12, payment_type="1", address="RktnqF7tw8fggwfx6p44b1VNjMgB14sFtJ",
        discord_id="388984890076692481", this_month="10", this_year="2017", txid="")
    octMinMc2 = Payment(coin="BITB", date="1510185600", amount=3434.13, payment_type="1", address="2Ff2vYvuZyAG2HTNucRiYchAG2xcPqDqZH",
        discord_id="388984890076692481", this_month="10", this_year="2017", txid="")
    octMinMc3 = Payment(coin="OK", date="1510185600", amount=17.79, payment_type="1", address="PWxzuWiahngSkdEV4H381V9Vk2yDYm1Ev7",
        discord_id="388984890076692481", this_month="10", this_year="2017", txid="")

    s.add(octMinJaws)
    s.add(octMinWolf)
    s.add(octMinKal)
    s.add(octMinRio)
    s.add(octMinBaby)
    s.add(octMinMc1)
    s.add(octMinMc2)
    s.add(octMinMc3)

    octStJaws = Payment(coin="DOGE", date="1509494400", amount=235.99,  address="AAFwzhTtsu3ouxNrqJdatUwST1hG6VDkds",
        discord_id="264272631560142848", this_month="10", this_year="2017", txid="")
    octStWolf = Payment(coin="DOGE", date="1509494400", amount=81.13,  address="A7dMpeqdeAKr3jpmFofjzxT91yZ14kc6xu",
        discord_id="311809553626824714", this_month="10", this_year="2017", txid="")
    octStKal = Payment(coin="DOGE", date="1509494400", amount=64.7,  address="A8egt413qHHi5K6frEwqPxX1DPPouKQNyP",
        discord_id="273550198691856394", this_month="10", this_year="2017", txid="")
    octStRio = Payment(coin="DOGE", date="1509494400", amount=81.13,  address="9rzMvprbcf9927TeYQGA9tpb6Y3ZvsVcLo",
        discord_id="210913059231760384", this_month="10", this_year="2017", txid="")



    s.add(octStJaws)
    s.add(octStWolf)
    s.add(octStKal)
    s.add(octStRio)


    s.commit()

popdb()