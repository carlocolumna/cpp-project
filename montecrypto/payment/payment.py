from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
import time

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///C:\\Users\\carLo_\\bots\\db\\db2.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
format = "%d-%m-%Y"

class Payment(db.Model):
	__tablename__ = "payment"
	id = db.Column('id', db.Integer, primary_key=True)
	date = db.Column('date', db.Integer)
	coin = db.Column('name', db.String(25))
	amount = db.Column('amount', db.Float)
	payment_type = db.Column('payment_type', db.Boolean,
						  default=False)  # False-payments from staking, True-payments from Mining
	address = db.Column('address', db.String(255))
	discord_id = db.Column('discord_id', db.Integer)
	this_month = db.Column('this_month', db.Integer)
	this_year = db.Column('this_year', db.Integer)
	txid = db.Column('txid', db.String(255))


@app.route('/')
def index():
	return render_template('index.html')

@app.route('/process', methods=['POST'])
def process():
	date = request.form['Date']
	coin = request.form['Coin']
	amount = request.form['Amount']
	txid = request.form['Txid']
	paymenttype = request.form['PaymentType']
	address = request.form['Address']
	month = request.form['MonthPeriod']
	year = request.form['YearPeriod']
	discordid = request.form['DiscordID']
	record = Payment(coin=coin, date=time.mktime(time.strptime(date, format)), amount=amount, payment_type=paymenttype,
						 address=address, discord_id=discordid, this_month=month, this_year=year, txid=txid)
	db.session.add(record)
	db.session.commit()

	return redirect(url_for('index'))


if __name__ == '__main__':
	app.run(debug=True)