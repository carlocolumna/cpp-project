#mcbot_members = ('indiana jaws','kali','pixel bits','b4dw0lf') 
mcbot_members = {
					'264272631560142848': 'Indiana JaWs',
					'273550198691856394':'kaLi',
					'157024659546701824':'PixelBits',
					'311809553626824714':'b4dw0lf',
					'388984890076692481': 'Monte Crypto',
					'311058941150756864': 'baby.ai73',
					'210913059231760384': 'riio'

				}
mcbot_coins = ("1337","BITB", "INFO", "EMB")
mcbot_core_channel = "311420800559218689"
mcbot_server_id = "311056219127414784"

def check(memberid):
    status = [False,"You are not allowed to use this command"]
    for k, v  in mcbot_members.items():
        if k == str(memberid):
        	status[0] = True
    return status

def check_coin(symbol):
	status = False
	try:
		coin = str(symbol)
	except Exception:
		return status
	
	if coin in mcbot_coins:
		status = True
	return status

def get_username(memberid):
	for k, v  in mcbot_members.items():
		if k == str(memberid):
			return v
