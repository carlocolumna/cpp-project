from sqlalchemy import create_engine, Column, Integer, String, Boolean, Float, ForeignKey ,exc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker,relationship

Base = declarative_base()

class Coin(Base):
    __tablename__ = "coin"
    id = Column('id', Integer, primary_key=True)
    symbol = Column('symbol', String(10), unique=True)
    name = Column('name', String(25), unique=True)
    rpc_port = Column('rpc_port', Integer, unique=True)
    version = Column('version', String(50))
    tier = Column('tier', Integer)
    status = Column ('status', String(25))
    online = Column('online', Boolean, default = False)
    wallets = relationship('Wallet' , backref='this_coin', lazy='dynamic')

class Wallet(Base):
    __tablename__ = "wallet"
    id = Column('id', Integer, primary_key=True)
    wallet_type = Column('wallet_type', Boolean, default=False)
    address = Column('address', String(255), unique=True)
    discord_id = Column('discord_id', Integer, unique=False)
    coin_id = Column(Integer, ForeignKey('coin.id'))
    deposits = relationship('Deposit', backref='this_wallet', lazy='dynamic')

class Deposit(Base):
    __tablename__ = "deposit"
    id = Column ('id', Integer, primary_key=True)
    amount = Column('amount', Float)
    amount_type = Column('amount_type', Boolean, default=False)
    txid = Column('txid', String(255), unique=True)
    chksum = Column('chksum', String(255), unique=True)
    date =  Column('date', Integer)
    this_month = Column('this_month',Integer)
    this_year = Column('this_year', Integer)
    received_at = Column('received_at', Integer)
    wallet_id = Column(Integer, ForeignKey('wallet.id'))


