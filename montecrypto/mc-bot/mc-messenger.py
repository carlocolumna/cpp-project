import discord
from discord.ext import commands
import config as cfg
import asyncio
from datetime import datetime
import time
import logging
import model
import api

logging.basicConfig(filename='errors.log', level=logging.INFO)
mc_messenger = commands.Bot(command_prefix=cfg.mcbot_prefix)

# DB hooks
engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=False)
session_maker = model.sessionmaker(bind=engine)
session = session_maker()


@mc_messenger.event
@asyncio.coroutine
def on_ready():
    print('---------')
    print('Logging in as')
    print(mc_messenger.user.name)
    print(mc_messenger.user.id)
    print('---------')


@asyncio.coroutine
def get_latest_tx():
    yield from mc_messenger.wait_until_ready()
    channel = discord.Object(id=cfg.mcbot_core_channel_id)
    last_check = (time.mktime(datetime.now().timetuple())) - 20000

    while not mc_messenger.is_closed:

        results, last_check = api.get_last_transactions(model, session, last_check)
        if results:
            embed = discord.Embed(title="This is a Broadcast Message", description="\u200b", color=0xff8040)
            embed.set_footer(text="Delivered By " + mc_messenger.user.name + " the " + str(datetime.now()))
            tx = ""
            for i in results:
                user_id = "<@" + str(i["Member"]) + ">"
                type = u'\U0001F4B0' if i["Type"] else u'\U0001F48E'
                tx = tx + type + " : " + "`" + i["Date"] + " | " + "{0:.3f} ".format(i["Amount"]) + i["Coin"] + "`" + " ---> " + user_id + "\n"
            embed.add_field(name="Last Deposits/Stakes", value=tx, inline=False)
            #embed.add_field(name="test", value=str(results))
            yield from mc_messenger.send_message(channel, embed=embed)
        else:
            print("nothing new")
        print("Last signal : {} @ {}".format(last_check,str(datetime.now())))
        yield from asyncio.sleep(60)


mc_messenger.loop.create_task(get_latest_tx())
mc_messenger.run(cfg.mcbot_token)