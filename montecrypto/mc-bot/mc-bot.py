import discord
from discord.ext import commands
import config as cfg
import permissions as sec
import asyncio
import datetime
import logging
import api
from discord.ext.commands import Bot


logging.basicConfig(level=logging.INFO)
mcbot = commands.Bot(command_prefix = cfg.mcbot_prefix)


@mcbot.event
@asyncio.coroutine
def on_ready():
    print("Monte Crypto BOT is now online")
    print("Name : {}".format(mcbot.user.name))

@mcbot.command(pass_context=True)
@asyncio.coroutine
def sos(ctx):
    check = sec.check(ctx.message.author.id)
    if check[0]:
        embed = discord.Embed(title="MC-BOT HELP SECTION", description="List of all commands offered by MC-BOT", color=0xff8040)
        embed.set_footer(text="Request Delivered By " + mcbot.user.name + " the " + str(datetime.datetime.now()))
        embed.add_field(name="!sos", value="provide a list of all supported commands", inline=False)
        embed.add_field(name="!version", value="retrieve bot information", inline=False)
        embed.add_field(name="!balance [option]", value="[option : me] Get Historical Balances for Monte Crypto or my wallets", inline=False)
        embed.add_field(name="!account [symbol]", value="[symbol : 1337, BITB, EMB] List wallet addresses", inline=False)
        yield from mcbot.say(embed=embed)
    else:
        yield from mcbot.say("<@{0.author.id}> , {1}".format(ctx.message,check[1]))


@mcbot.command(pass_context=True)
@asyncio.coroutine
def version(ctx):
    embed = discord.Embed(title="Monte Crypto Bot", description="Versioning Bot Information", color=0xff8040)
    embed.set_author(name="Author : Indiana JaWs",icon_url= mcbot.user.avatar_url)
    embed.set_footer(text="Request Delivered By "+ mcbot.user.name + " the " + str(datetime.datetime.now()))
    embed.add_field(name="Version", value="0.1", inline=True)
    embed.add_field(name="Last Update", value="10-DEC-17", inline=True)
    embed.add_field(name="Codename", value="LightSource", inline=True)
    yield from mcbot.say(embed=embed)


@mcbot.command(pass_context=True)
@asyncio.coroutine
def address(ctx, text : str=None):
    check = sec.check(ctx.message.author.id)
    if check[0]:
        data = api.get_address(text, mcbot.user.id, ctx.message.author.id)
        if data:    
            embed = discord.Embed(title= "Address", description="\u200b", color=0xff8040)
            embed.set_author(name=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
            embed.set_footer(text="Request Delivered By " + mcbot.user.name + " the " + str(datetime.datetime.now()))
            for item in data:
                embed.add_field(name=item["coin"], value=item["address"], inline=False)
            yield from mcbot.say(embed=embed)
        else:
            yield from mcbot.say("<@{0.author.id}> , {1}".format(ctx.message," You must enter a valid option or a coin symbol, current suported symbols : 1337, BITB, EMB"))
    else:
        yield from mcbot.say("<@{0.author.id}> , {1}".format(ctx.message, check[1]))

@mcbot.command(pass_context=True)
@asyncio.coroutine
def balance(ctx, member : str=None):
    check = sec.check(ctx.message.author.id)
    if check[0]:
        if member != None:
            if member.lower() == "me":
                data = api.get_balance_members(ctx.message.author.id)
                embed = discord.Embed(title="My Community Wallets Balances", description="My Historical Balances", color=0xff8040)
                embed.set_author(name= ctx.message.author, icon_url=ctx.message.author.avatar_url)
            else:
                yield from mcbot.say("<@{0.author.id}> , {1}".format(ctx.message," You must enter a valid option"))
        else:
            data = api.get_balance()
            embed = discord.Embed(title="Monte Crypto Community Wallets Balances", description="Historical Balances from all Core Members", color=0xff8040)
            embed.set_author(name="Author : Monte Crypto")
        embed.set_footer(text="Request Delivered By " + mcbot.user.name + " the " + str(datetime.datetime.now()))
        for item in data:
            embed.add_field(name= item["coin"] , value= "{0:.3f} ".format(float(item["deposit"])), inline=True)
        embed.add_field(name='\u200b', value= '\u200b', inline=True)
        yield from mcbot.say(embed=embed)
    else:
        yield from mcbot.say("<@{0.author.id}> , {1}".format(ctx.message, check[1]))

@mcbot.command(pass_context=True)
@asyncio.coroutine
def deposit(ctx, *args):
    check = sec.check(ctx.message.author.id)
    if check[0]:
        text = ' '.join(args)
        data = api.get_deposit(text, mcbot.user.id, ctx.message.author.id)
        if data:    
            embed = discord.Embed(title= "", description="\u200b", color=0xff8040)
            embed.set_author(name=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
            embed.set_footer(text="Request Delivered By " + mcbot.user.name + " the " + str(datetime.datetime.now()))
            tx = ""
            for item in data:
                user_id = sec.get_username(item['userID'])
                deposit_icon = u'\U0001F4B0'
                tx = tx + deposit_icon + " : " + "`" + item['date'] + " | " + "{0:.3f} ".format(item['amount']) + item['coin'] + "`" + " ---> " + user_id + "\n"
                #name = yield from mcbot.get_user_info(item['userID'])
                #print(name.display_name)
            embed.add_field(name= "Latest Deposits", value=tx, inline=False)
            yield from mcbot.say(embed=embed)
        else:
            yield from mcbot.say("<@{0.author.id}> , {1}".format(ctx.message," You must enter a valid option or a coin symbol"))
    else:
        yield from mcbot.say("<@{0.author.id}> , {1}".format(ctx.message, check[1]))

mcbot.run(cfg.mcbot_token)