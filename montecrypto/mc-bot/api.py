# import simplejson
import os
import model
import logging
import time
import simplejson
from datetime import datetime
from sqlalchemy.sql import func
import permissions as sec

logging.basicConfig(filename='errors.log', level=logging.ERROR)

engine = model.create_engine('sqlite:///C:\\Users\\carLo_\\bots\\db\\mc-db.db', echo=True)
#engine = model.create_engine('sqlite:///M:\\mc-db.db', echo=True)
# model.Base.metadata.create_all(bind=engine)

# create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()


def get_balance():
    data = []
    results = session.query(func.sum(model.Deposit.amount), 
        model.Coin.symbol).join(model.Wallet).join(model.Coin).filter(model.Deposit.amount_type==
        1).group_by(model.Wallet.coin_id)

    # results2 = session.query(func.sum(model.Deposit.amount), model.Deposit.date, 
    #     model.Coin.symbol, model.Deposit.amount_type).join(model.Wallet).join(model.Coin).filter(model.Deposit.amount_type==
    #     0, model.Coin.symbol=="INFO").group_by(model.Wallet.coin_id)

    # for row in results2:
    #     print(row)

    for row in results:
        data.append(
            {
                'deposit': str(row[0]),
                "coin"  :   row[1]

            })
    print(data)
    return data

# get_balance()

def get_balance_members(member : str):
    data = []
    results = session.query(func.sum(model.Deposit.amount), model.Coin.symbol).join(model.Wallet).join(model.Coin).filter(model.Deposit.amount_type==
        1, model.Wallet.discord_id == int(member)).group_by(model.Coin.symbol)
    for row in results:
        data.append(
            {
                "deposit": str(row[0]),
                "coin"  :   row[1]

            })


    print(data)
    return data


def get_address(text : str, botID : str, memberID : str):
    data = []

    if text != None:
        if text.upper() == "ME":
            results = session.query(model.Wallet.address, 
            model.Coin.symbol).join(model.Coin).filter(model.Wallet.discord_id==int(memberID))
        elif sec.check_coin(text.upper()):
            results = session.query(model.Wallet.address, 
            model.Coin.symbol).join(model.Coin).filter(model.Wallet.discord_id==int(memberID), model.Coin.symbol==text.upper())
    else:
        results = session.query(model.Wallet.address, 
            model.Coin.symbol).join(model.Coin).filter(model.Wallet.discord_id==int(botID))
        #"311809553626824714"
    if results != None:
        for row in results:
            data.append(
                {
                    "address": row[0],
                    "coin"   : row[1]
                })

    print(data)
    return data

def get_deposit(text : str, botID : str, memberID : str):

    if text != "":
        print("A")
        textList = text.split()
        if len(textList) > 1:
            if textList[0].upper() == "ME" and sec.check_coin(textList[1].upper()):
                print("G")
                results = session.query(model.Deposit.amount, model.Wallet.discord_id,  
                    model.Coin.symbol).join(model.Wallet).join(model.Coin).filter(model.Wallet.discord_id==int(memberID), 
                    model.Coin.symbol==textList[1].upper(), model.Deposit.amount_type==1).order_by(model.Deposit.date.desc()).limit(12)
            
            elif textList[0].upper() == "MC" and sec.check_coin(textList[1].upper()):
                print("F")
                results = session.query(model.Deposit.amount, model.Wallet.discord_id,  
                    model.Coin.symbol).join(model.Wallet).join(model.Coin).filter(model.Wallet.discord_id==int(botID), 
                    model.Coin.symbol==textList[1].upper(), model.Deposit.amount_type==1).order_by(model.Deposit.date.desc()).limit(12)

            else:
                print("B")
                return data
        else:
            if sec.check_coin(text.upper()):
                print("H")
                results = session.query(model.Deposit.amount, model.Deposit.date, model.Wallet.discord_id, model.Coin.symbol).join(model.Wallet).join(model.Coin).filter(model.Coin.symbol
                    ==text.upper(), model.Deposit.amount_type==1).order_by(model.Deposit.date.desc()).limit(12)
            
            elif text.upper() == "ME":
                print("I")
                results = session.query(model.Deposit.amount, model.Deposit.date, model.Wallet.discord_id, model.Coin.symbol).join(model.Wallet).join(model.Coin).filter(model.Wallet.discord_id
                    ==int(memberID), model.Deposit.amount_type==1).order_by(model.Deposit.date.desc()).limit(12)

            elif text.upper() == "MC":
                print("E")
                results = session.query(model.Deposit.amount, model.Deposit.date, model.Wallet.discord_id, model.Coin.symbol).join(model.Wallet).join(model.Coin).filter(model.Wallet.discord_id
                    ==int(botID), model.Deposit.amount_type==1).order_by(model.Deposit.date.desc()).limit(12)

            else:
                print("C")
                return data
    else:
        print("J")
        results = session.query(model.Deposit.amount, model.Deposit.date, model.Wallet.discord_id, model.Coin.symbol).join(model.Wallet).join(model.Coin).filter(model.Deposit.amount_type
            ==1).order_by(model.Deposit.date.desc()).limit(12)

    
    data = []

    if results != None:
        for row in results:
            data.append(
                {
                    "userID":   row[2],
                    "coin":     row[3],
                    "amount":   row[0],
                    "date":     time.strftime("%d-%b-%y @ %H:%M", time.localtime(row[1]))
                })

           

    print(data)
    return data

#get_deposit("","2323","273550198691856394")
