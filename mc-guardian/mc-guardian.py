import discord
from discord.ext import commands
import config as cfg
#import permissions as sec
import asyncio
import datetime
import logging
import api


logging.basicConfig(level=logging.INFO)
#client = discord.Client()
mcguardian = commands.Bot(command_prefix = cfg.mcbot_prefix)

@asyncio.coroutine
def my_background_task():
    yield from mcguardian.wait_until_ready()
    channel = discord.Object(id= cfg.mcbot_core_channel_id)
    while not mcguardian.is_closed:
        data = api.get_status()
        embed = discord.Embed(title="This is a Broadcast Message", description="\u200b", color=0xff8040)
        embed.set_footer(text="Delivered By " + mcguardian.user.name + " the " + str(datetime.datetime.now()))
        offline_pc = ""
        for k, v in data.items():
            offline_pc = offline_pc + k + " : " + v + "\n"
        embed.add_field(name="Wellington Offline Miners", value=offline_pc, inline=False)

        if not data.get('Status'):
            yield from mcguardian.send_message(channel,embed=embed)

        yield from asyncio.sleep(3600) # task runs every hour

@mcguardian.event
@asyncio.coroutine
def on_ready():
    print('------')
    print('Logged in as')
    print(mcguardian.user.name)
    print(mcguardian.user.id)
    print('------')

@mcguardian.command(pass_context=True)
@asyncio.coroutine
def status(ctx):
    data = api.get_status()
    embed = discord.Embed(title= "OFFLINE MINERS", description="\u200b", color=0xff8040)
    embed.set_footer(text="Request Delivered By " + mcguardian.user.name + " the " + str(datetime.datetime.now()))

    offline_pc = ""
    for k, v in data.items():
        offline_pc = offline_pc + k + " : " + v + "\n"
    embed.add_field(name="Wellington Offline Miners", value=offline_pc, inline=False)
    yield from mcguardian.say(embed=embed)

mcguardian.loop.create_task(my_background_task())
mcguardian.run(cfg.mcbot_token)