import model
from datetime import datetime
from sqlalchemy import func, case, and_
import config as cfg
import urllib.request, json

# create DB mapping
#engine = model.create_engine('sqlite:///C:\\Users\\carLo_\\bots\\db\\mc-db.db', echo=False)
engine = model.create_engine('sqlite:///C:\\Users\\kaLi\\Documents\\CPP-Project\\bots\\db\\mc-db.db', echo=False)

# create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()


def get_permissions_list():
    # get list of dicord_id users
    qry = session.query(model.Wallet.discord_id).distinct().all()

    # stroe in a list
    results = []

    for discord_user in qry:
        results.append(str(discord_user.discord_id))

    return results


def get_coins_list():
    # get coin symbol and id, return as a list of dict
    qry = session.query(model.Coin.id, model.Coin.symbol, model.Coin.name).filter(model.Coin.online == True).all()

    # store in a list of dict
    results = []

    for coin in qry:
        results.append(
            {
                'coin_id': coin.id,
                'coin_symbol': coin.symbol,
                'coin_name': coin.name
            }
        )

    return results


def get_my_balance(discord_id):
    # get sum of all my crypto balances
    qry = session.query(model.Coin.name,model.Coin.tier, model.Coin.symbol,
                        func.sum(case([(model.Deposit.amount_type == False, model.Deposit.amount)], else_=0)).label(
                            'total')).outerjoin(
        model.Wallet).outerjoin(model.Deposit).filter(model.Wallet.discord_id == discord_id).group_by(
        model.Coin.id).all()

    return qry


def get_mc_balance():
    qry = session.query(model.Coin.name, model.Coin.symbol, model.Coin.tier, func.sum(model.Deposit.amount).label('total')).join(
        model.Wallet).join(model.Deposit).filter(model.Deposit.amount_type == False).group_by(model.Coin.id).all()

    return qry


# return member total balance for a coin
def get_my_total_coin_balance(discord_id, coin_symbol):
    qry = session.query(model.Coin.id, model.Coin.name, model.Coin.symbol,
                        func.sum(case([(model.Deposit.amount_type == False, model.Deposit.amount)], else_=0)).label(
                            'Total Balance')).outerjoin(
        model.Wallet).outerjoin(model.Deposit).filter(model.Wallet.discord_id == discord_id).filter(
        model.Coin.symbol == str(coin_symbol)).all()

    return qry


# return member total balance for a coin in a historical grouping of tx
def get_my_coin_balance(didscord_id, coin_id):
    qry = session.query(model.Coin.name, model.Coin.symbol, model.Deposit.this_month, model.Deposit.this_year,
                        func.sum(case([(model.Deposit.amount_type == False, model.Deposit.amount)], else_=0)).label(
                            'Total Deposits'),
                        func.sum(case([(model.Deposit.amount_type == False, 1)], else_=0)).label(
                            'Desposits Count')).join(model.Wallet).join(
        model.Deposit).filter(model.Coin.id == coin_id).filter(
        model.Wallet.discord_id == didscord_id).group_by(model.Deposit.this_month,
                                                         model.Deposit.this_year).order_by(
        model.Deposit.this_year.desc(), model.Deposit.this_month.desc()).all()

    return qry


# return Monte Crypto total balance for a coin
def get_mc_total_coin_balance(coin_symbol):
    qry = session.query(model.Coin.id, model.Coin.name, model.Coin.symbol,
                        func.sum(case([(model.Deposit.amount_type == False, model.Deposit.amount)], else_=0)).label(
                            'Total Balance')).outerjoin(
        model.Wallet).outerjoin(model.Deposit).filter(model.Coin.symbol == str(coin_symbol)).all()

    return qry


# return Monte Crypto total balance for a coin in a historical grouping of tx
def get_mc_coin_balance(coin_id):
    qry = session.query(model.Coin.name, model.Coin.symbol, model.Deposit.this_month, model.Deposit.this_year,
                        func.sum(case([(model.Deposit.amount_type == False, model.Deposit.amount)], else_=0)).label(
                            'Total Deposits'),
                        func.sum(case([(model.Deposit.amount_type == False, 1)], else_=0)).label(
                            'Desposits Count')).join(model.Wallet).join(
        model.Deposit).filter(model.Coin.id == coin_id).filter(
        model.Coin.id == coin_id).group_by(model.Deposit.this_month,
                                           model.Deposit.this_year).order_by(
        model.Deposit.this_year.desc(), model.Deposit.this_month.desc()).all()

    return qry


# return stakes for the month
def get_month_stakes():
    qry = session.query(model.Coin.name, model.Coin.symbol,
                        func.sum(case([(model.Deposit.amount_type == True, model.Deposit.amount)], else_=0)).label(
                            'Total Stakes')).outerjoin(
        model.Wallet).outerjoin(model.Deposit).filter(model.Deposit.this_month == datetime.now().month).filter(
        model.Deposit.this_year == datetime.now().year).group_by(model.Coin.id).all()

    print(qry)
    return qry

#get_month_stakes()

def get_mc_stakes_by_coin(coin_symbol):
    qry = session.query(model.Coin.name, model.Coin.symbol, model.Coin.tier, model.Deposit.amount, model.Deposit.received_at,
                        model.Wallet.discord_id).join(model.Wallet).join(
        model.Deposit).filter(model.Deposit.amount_type == True).filter(model.Coin.symbol == coin_symbol).order_by(
        model.Deposit.received_at.desc()).limit(10).all()

    return qry

def get_payments_by_month(discord_id, month, payment_type):

    qry = session.query(model.Payment.date, model.Payment.amount, model.Payment.coin, model.Payment.address, model.Payment.this_year).filter(
        model.Payment.payment_type == payment_type).filter(model.Payment.discord_id == discord_id).filter(
        model.Payment.this_month == month).order_by(model.Payment.coin.asc()).all()

    print(qry)
    return qry

#get_payments(273550198691856394, 12, True)

def get_payments_by_year(discord_id, year, payment_type):

    qry = session.query(model.Payment.date, model.Payment.amount, model.Payment.coin, model.Payment.address).filter(
        model.Payment.payment_type == payment_type).filter(model.Payment.discord_id == discord_id).filter(
        model.Payment.this_year == year).order_by(model.Payment.coin.asc()).all()

    print(qry)
    return qry


#return member wallet addresses
def get_addresses(discord_id):
    qry = session.query(model.Coin.name, model.Wallet.address).join(model.Wallet).filter(
        model.Wallet.discord_id == discord_id).order_by(model.Coin.name.asc()).all()

    return qry


def get_my_reward(discord_id):
    qry = session.query(model.Coin.name, model.Coin.symbol,
                        func.sum(case([(and_(model.Deposit.amount_type == False
                                             #model.Wallet.discord_id != cfg.mcbot_community
                                             ),
                                        model.Deposit.amount / 12)], else_=0)).label(
                            "Balance_Target"),
                        func.sum(case([(and_(model.Deposit.amount_type == False,
                                             model.Wallet.discord_id == discord_id
                                             ),
                                        model.Deposit.amount)], else_=0)).label(
                            "User_Total_Balance"),
                        func.sum(case([(and_(model.Deposit.this_month == datetime.now().month,
                                             model.Deposit.this_year == datetime.now().year,
                                             model.Deposit.amount_type == True,
                                             ),
                                        model.Deposit.amount / 2 / 6)], else_=0)).label(
                            "Monthly_Stakes"),
                        func.sum(case([(and_(model.Deposit.this_month == datetime.now().month,
                                             model.Deposit.this_year == datetime.now().year,
                                             model.Deposit.amount_type == False,
                                             model.Wallet.discord_id == discord_id
                                             ),
                                        1)], else_=0)).label("Month"),
                        func.sum(case([(and_
                                        (model.Deposit.this_month == datetime.now().month,
                                         model.Deposit.this_year == datetime.now().year,
                                         model.Deposit.amount_type == False,
                                         model.Wallet.discord_id == discord_id
                                         ),
                                        model.Deposit.amount)], else_=0)).label(
                            'User_Monthly_Deposit')).outerjoin(
        model.Wallet).outerjoin(model.Deposit).filter(model.Coin.online == True).group_by(model.Coin.id).all()

    return qry


def get_account_tx(a):
    qry = session.query(model.Coin.name, model.Wallet.address, model.Deposit.amount, model.Deposit.txid).join(
        model.Wallet).join(model.Deposit).filter(model.Wallet.address == a).all()

    return qry


def get_my_deposits(discord_id):
    qry = session.query(model.Coin.name, model.Coin.symbol,model.Coin.tier, model.Deposit.amount, model.Deposit.received_at).join(
        model.Wallet).join(
        model.Deposit).filter(model.Deposit.amount_type == False).filter(
        model.Wallet.discord_id == discord_id).order_by(model.Deposit.received_at.desc()).limit(10).all()

    return qry


def get_my_deposits_by_coin(discord_id, coin_symbol):
    qry = session.query(model.Coin.name, model.Coin.symbol, model.Coin.tier, model.Deposit.amount, model.Deposit.received_at).join(
        model.Wallet).join(
        model.Deposit).filter(model.Deposit.amount_type == False).filter(model.Wallet.discord_id == discord_id).filter(
        model.Coin.symbol == coin_symbol).order_by(model.Deposit.received_at.desc()).limit(10).all()

    return qry




def get_mc_deposits():
    qry = session.query(model.Coin.name, model.Coin.symbol, model.Coin.tier, model.Deposit.amount, model.Deposit.received_at,
                        model.Wallet.discord_id).join(model.Wallet).join(
        model.Deposit).filter(model.Deposit.amount_type == False).order_by(model.Deposit.received_at.desc()).limit(
        10).all()

    return qry


def get_mc_deposits_by_coin(coin_symbol):
    qry = session.query(model.Coin.name, model.Coin.symbol,model.Coin.tier, model.Deposit.amount, model.Deposit.received_at,
                        model.Wallet.discord_id).join(model.Wallet).join(
        model.Deposit).filter(model.Deposit.amount_type == False).filter(model.Coin.symbol == coin_symbol).order_by(
        model.Deposit.received_at.desc()).limit(10).all()

    return qry


def get_targets(discord_id):
    qry = session.query(model.Coin.name, model.Coin.symbol, func.sum(model.Deposit.amount).label('total')).join(
        model.Wallet).join(model.Deposit).filter(model.Deposit.amount_type == False).filter(
        model.Wallet.discord_id != discord_id).group_by(model.Coin.id).all()

    return qry

# return the community coin's current prices for the month
def get_prices():
    # qry = session.query(model.Coin.name).all()
    # print(qry)
    data = []
    coinList = ["1337", "Bitbean","Embercoin", "Insanecoin-insn", "OKCash", "Radium", "Reddcoin", "Social-Send", "Stratis" ]
    #changes to be made at db: Embercoin, Insanecoin-insn, Social-Send, no Infocoin on coinmarketcap

    for coin in coinList:
        coin_url = "https://api.coinmarketcap.com/v1/ticker/" + coin.lower() + "/"
        with urllib.request.urlopen(coin_url) as url:
            response = json.loads(url.read().decode())
            
            for item in response:
                data.append({

                    "coin": item["name"],
                    "price_btc": item["price_btc"],
                    "price_usd": item["price_usd"],
                    "24h_change": item["percent_change_24h"],
                    "7d_change": item["percent_change_7d"]
                    })
            #print(response)
    #print(data)
    return data
      
#get_prices()


def get_rank(coin_symbol):
    qry = session.query(model.Coin.name, model.Coin.symbol, model.Wallet.discord_id,
                        func.sum(case([(model.Deposit.amount_type == False, model.Deposit.amount)], else_=0)).label('total')).outerjoin(model.Wallet).outerjoin(
        model.Deposit).filter(model.Coin.symbol == coin_symbol).group_by(model.Wallet.id).order_by(func.sum(case([(model.Deposit.amount_type == False, model.Deposit.amount)], else_=0)).desc()).all()

    return qry

# ranks = get_rank('OK')
# sum = sum((total[3] for total in ranks))
# print(sum)
# for rank in ranks:
#     print(rank)
#     percent = ((rank[3] * 100 ) / sum)
#     bars = int(round(((( percent * 25) / 100))))
#     print("{0} {1: .2f}%".format("|"*bars, percent))
# targets = get_targets(388984890076692481)
#
# for target in targets:
#     print("target is {0: 10,.3f}".format((target[2] / 12)))
# print(get_mc_deposits())

# print(get_my_deposits(264272631560142848))

# print(get_my_deposits_by_coin(264272631560142848, 'SEND'))
# result = get_account_tx('SZShNyhNxCVLE9YyxTphA8C1dRzfDphgcc')

# for tx in result:
#     print("amount : {0: 15,.3} | {1:}".format(tx.amount, tx.txid))
# print(get_my_balance(264272631560142848))
# print(get_my_balance(157024659546701824))
# print(get_my_total_coin_balance(157024659546701824, 'OK'))
# print(get_my_coin_balance(264272631560142848, 1))

print(u'\u25B6'   u'\u2540')
