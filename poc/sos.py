import discord
from discord.ext import commands
import logging
import config as cfg
import asyncio
import time
from datetime import datetime
import unicodedata
import calendar
from calendar import month_name

# \u200b - blank carriage

logging.basicConfig(filename='errors.log', level=logging.ERROR)

# create bot
cpp_student = commands.Bot(command_prefix=cfg.studentbot_prefix)

cmd_list = ('grade', 'qual', 'summary', 'sum', 'review', 'stud', 'cohort', 'feb18', 'date')

grade_list = ({'IT Systems':{'Workshop': '85', 'Project': '90', 'Presentation': '80', 'Exam': '92'}},
               {'Data Handling': {'Project 1': '80', 'Project 2': '86', 'Task 1': '91', 'Task 2': '90', 'Exam': '82'}},
               {'Professional Practice': {'Workshop': '85', 'Project': '84', 'Task': '90', 'Exam': '92'}},
               {'Programming Principles': {'Workshop': '83', 'Project': '80', 'Task 1': '83', 'Task 2': '90', 'Exam': '92'}},
               {'Computer Servicing': {'Workshop': '92', 'Project': '85', 'Presentation': '89', 'Exam': '83'}},
               {'Operating Systems': {'Project': '86', 'Task': '90', 'Exam': '88'}},
               {'Networking': {'Workshop': '80', 'Project': '85', 'Exam': '77'}},
               {'System Administration': {'Task': '81', 'Presentation': '75', 'Exam': '83'}})

date_list = ({'IT Systems':{'Workshop': '18 Apr 2018 0900H', 'Project': '18 Apr 2018', 'Presentation': '18 Apr 2018 0900H', 'Exam': '18 Apr 2018 0900H'}},
               {'Data Handling': {'Project 1': '18 Apr 2018', 'Project 2': '18 Apr 2018','Exam': '18 Apr 2018 0900H'}},
               {'Professional Practice': {'Workshop': '18 Apr 2018 0900H', 'Project': '18 Apr 2018', 'Exam': '18 Apr 2018 0900H'}},
               {'Programming Principles': {'Workshop': '18 Apr 2018 0900H', 'Project': '18 Apr 2018', 'Exam': '18 Apr 2018 0900H'}},
               {'Computer Servicing': {'Workshop': '18 Apr 2018 0900H', 'Project': '18 Apr 2018', 'Presentation': '18 Apr 2018 0900H', 'Exam': '18 Apr 2018 0900H'}},
               {'Operating Systems': {'Project': '18 Apr 2018','Exam': '18 Apr 2018 0900H'}},
               {'Networking': {'Workshop': '18 Apr 2018 0900H', 'Project': '18 Apr 2018', 'Exam': '18 Apr 2018 0900H'}},
               {'System Administration': {'Presentation': '18 Apr 2018 0900H', 'Exam': '18 Apr 2018 0900H'}})

period_list = ({'IT Systems': '29 Jan 2018  -  12 Feb 2018'},
               {'Data Handling': '29 Jan 2018  -  12 Feb 2018'},
               {'Professional Practice': '29 Jan 2018  -  12 Feb 2018'},
               {'Programming Principles': '29 Jan 2018  -  12 Feb 2018'},
               {'Computer Servicing': '29 Jan 2018  -  12 Feb 2018'},
               {'Operating Systems': '29 Jan 2018  -  12 Feb 2018'},
               {'Networking': '29 Jan 2018  -  12 Feb 2018'},
               {'System Administration': '29 Jan 2018  -  12 Feb 2018'})

student_list = ({'Tom Sawyer' : {'StudentID':'91027986','Study':'Diploma in IT Technical Support Level 5', 'Current Course': 'IT Systems', 'Shift': 'Morning', 'Study Option':'Full-Time', 'Category': 'Domestic'}},
                {'Huckleberry Finn': {'StudentID':'91027986','Study':'Diploma in Web Development and Design Level 5', 'Current Course': 'Data Handling', 'Shift': 'Evening', 'Study Option':'Part-Time', 'Category': 'Domestic'}},
                {'Hannibal Lecter': {'StudentID':'91027986','Study':'Diploma in Software Development Level 6', 'Current Course': 'Computer Servicing', 'Shift': 'Afternoon', 'Study Option':'Part-Time', 'Category': 'International'}},
                {'Scarlett O\'Hara': {'StudentID':'91027986','Study':'Diploma in Advanced Network Engineering Level 7', 'Current Course': 'Networking', 'Shift': 'Evening', 'Study Option':'Full-Time', 'Category': 'Domestic'} },
                {'Jay Gatsby': {'StudentID':'91027986','Study':'Diploma in Systems Administration Level 6', 'Current Course': 'Systems Administration', 'Shift': 'Morning', 'Study Option':'Full-Time', 'Category': 'International'} })

inst_date_list = ({'February 2018':{'14 Feb 0900H': {'Workshop': 'Build a Desktop PC'}, '21 Feb 1400H': {'Presentation':'Network Security'}, '27 Feb 1500H': {'Workshop':'Build a Desktop PC'}}},
                {'March 2018': {'03 Mar 0800H': {'Workshop': 'Build a Desktop PC'}, '18 Mar 1300H': {'Presentation':'Network Security'}}},
               {'April 2018': {'21 Feb 1400H': {'Presentation':'Network Security'}, '10 Apr 1000H': {'Workshop': 'Build a Desktop PC'}}})


Q1 = 'You are creating a custom Distance class. You want to ease the conversion from your Distance class to a double. What should you add? \n' \
     'A. Nothing; this is already possible. \n' \
     'B. An implicit cast operator. \n' \
     'C. An explicit cast operator. \n' \
     'D. A static Parse method.'
Q1E = 'A. Incorrect: A conversion between a custom class and a value type does not exist by default. \n' \
      'B. Correct: Adding an implicit operator will enable users of your class to convert between Distance and double without any extra work. \n' \
      'C: Incorrect: Although adding an explicit cast operator will enable users of the class to convert from Distance to double, they will still need to explicitly cast it. \n' \
      'D: Incorrect: A Parse method is used when converting a string to a type. It doesn\'t add conversions from your type to another type. \n'

Q2 = 'You are creating a new collection type and you want to make sure the elements in it can be easily accessed. What should you add to the type? \n' \
     'A. Constructor \n' \
     'B. Indexer property \n' \
     'C. Generic type parameter \n' \
     'D. Static property'
Q2E = 'A: Incorrect: A constructor is used to create an instance of a new type \n' \
      'B. Correct: An indexer property enables the user of the type to easily access a type that represents an array-like collection. \n' \
      'C. Incorrect: Making the type generic enables you to store multiple different types inside your collection. \n' \
      'D. Incorrect: A static property cannot access the instance data of the collection.\n'

Q3 = 'You are creating a generic class that should work only with reference types. Which type constraint should you add? \n' \
     'A: where T: class \n' \
     'B. where T: struct \n' \
     'C. where T: new() \n' \
     'D. where T: IDisposable'

Q3E = 'A. Correct: Constraining your generic type parameter to class allows the class to be used only with reference type. \n' \
      'B. Incorrect: This will constrain the class to be used with a value type, not a reference type \n' \
      'C. Incorrect: This will constrain the class to be used with a type that has an empty default constructor. It can be both a value and a reference type. \n' \
      'D. Incorrect: This constrain the class to be used with a type that implements the IDisposable interface. \n'




question_list = ({'Q1' : { 'Question': Q1, 'B':Q1E }},
                {'Q2' : { 'Question': Q2, 'B':Q2E }},
                {'Q3' : { 'Question': Q3, 'A':Q3E }})



@cpp_student.event
async def on_ready():
    print('---------')
    print('Logging in as')
    print(cpp_student.user.name)
    print(cpp_student.user.id)
    print("CPP-Student online")
    print('---------')


@cpp_student.group(pass_context=True)
async def my(ctx):
    if ctx.invoked_subcommand is None or ctx.invoked_subcommand.name not in cmd_list:
        await cpp_student.say(
            "<@{}>, Invalid option..Supported options are ( {} )".format(ctx.message.author.id, ' | '.join(cmd_list)))

@my.group(pass_context=True)
async def b(ctx, cmd: str = 'nill'):
    print(cmd)
    if ctx.invoked_subcommand is None:
        await cpp_student.say("no coin entered")
    else:
        print(ctx.invoked_subcommand.name)
        await cpp_student.say(ctx.invoked_subcommand.name)



@b.command(pass_context=True)
async def balance(ctx):
    await cpp_student.say("balance")

@my.command(pass_context=True)
async def qual(ctx):
    embed = discord.Embed(title="",
                          description="", color=0xff8040)
    embed.set_author(
        name=ctx.message.author.name + "  |  Student ID 91027986 | Diploma in Systems Technology Level 5",
        icon_url=ctx.message.author.avatar_url)
    embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))

    record = ""
    for course in period_list:
        for k, v in course.items():
            record = record + "`" + "{0:} : {1: <25} {2: <30}".format(u'\U0001F4D2', k, v) + "`" + "\n"

    embed.add_field(name="{0: <25} \u2003 \u2003 \u2003 \u2003 {1: <40} \u2003 \u2003 \u2002 {2: <50}".format(
        "Summary of my Courses", "Start Date", "End Date"), value=record, inline=False)

    await cpp_student.send_message(ctx.message.author, embed=embed)

@my.command(pass_context=True)
async def sum(ctx):
    embed = discord.Embed(title="My Important Dates",
                          description="", color=0xff8040)
    embed.set_author(name=ctx.message.author.name + "  |  Student ID 91027986 | Diploma in Systems Technology Level 5",
                     icon_url=ctx.message.author.avatar_url)
    embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))

    data = {}
    for course in date_list:
        for firstKey in course:
            record = ""
            counter = 0
            for index, (secondKey, value) in enumerate(course[firstKey].items()):
                if counter == 0:
                    record = record + "`"
                record = record + "{0: <14} {1:<17}    ".format(secondKey, value)
                counter = counter + 1

                if counter == 2:
                    record = record + "`" + "\n"
                    counter = 0
                elif index == len(course[firstKey]) - 1:
                    record = record + "`" + "\n"
                    counter = 0

            data[firstKey] = record

    for cat in data:
        embed.add_field(name="{0:} : {1}".format(u"\U0001F4D6", cat), value=data[cat], inline=False)

    await cpp_student.send_message(ctx.message.author, embed=embed)

@my.command(pass_context=True)
async def review(ctx):
    if not str(ctx.message.channel).startswith("Direct Message with"):
        await cpp_student.say("`" + "Please proceed to your direct messaging channel with the bot." + "`")

    while True:
        await cpp_student.send_message(ctx.message.author, "`" + 'Press "g" to start the quiz' + "`")
        input = await cpp_student.wait_for_message(author=ctx.message.author)

        if input.clean_content.upper() == "G":
            count = 0
            point = 0
            for question in question_list:
                for k in question:
                    response = ""
                    count = count + 1
                    for i, v in question[k].items():
                        if response == "":
                            await cpp_student.send_message(ctx.message.author, "`" + "\u2753  " + i + " " + str(
                                count) + "  :  " + v + "`")
                            response = await cpp_student.wait_for_message(author=ctx.message.author)
                        else:

                            if response.clean_content.upper() == i:
                                await cpp_student.send_message(ctx.message.author,
                                                                    "`" + "\U0001F609 " + "You are correct! \n\n" + v + "`" + "\n\n \u200b")
                                response = ""
                                point = point + 1

                                while True:
                                    await cpp_student.send_message(ctx.message.author,
                                                                        "`" + "Press c to continue..." + "`")
                                    enter = await cpp_student.wait_for_message(author=ctx.message.author)
                                    if enter.clean_content.upper() == "C":
                                        break
                                    else:
                                        continue
                            else:
                                await cpp_student.send_message(ctx.message.author,
                                                                    "`" + "\U0001F61C " + 'You are wrong! The correct answer is ' + i + '\n\n' + v + "`" + "\n\n \u200b")
                                response = ""

                                while True:
                                    await cpp_student.send_message(ctx.message.author,
                                                                        "`" + "Press c to continue..." + "`")
                                    enter = await cpp_student.wait_for_message(author=ctx.message.author)
                                    if enter.clean_content.upper() == "C":
                                        break
                                    else:
                                        continue
            score = float(point) / float(count) * 100
            if score > 50:
                remark = "You passed! \U0001F389"
            else:
                remark = "You failed! \U0001F648 Please do more revisions."
            await cpp_student.send_message(ctx.message.author,
                                                "`" + "Quiz has ended.\n" + "Your score: {0: .2f}%\n".format(
                                                    score) + remark + "`")
            break
        else:
            continue

@my.command(pass_context=True)
async def stud(ctx):
    embed = discord.Embed(title="",
                          description="", color=0xff8040)
    embed.set_author(
        name=ctx.message.author.name + "  |  Lorem ipsum dolor sit amet | consectetur adipiscing elit, sed do eiusmod",
        icon_url=ctx.message.author.avatar_url)
    embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))

    data = {}
    for student in student_list:
        for name in student:
            record = ""
            for key, value in student[name].items():
                record = record + "`" + "{0:>3} {1:<15} : {2: <20}".format(u"\u26AA", key, value) + "`" + "\n"
            data[name] = record



    for cat in data:
        embed.add_field(name="{0:} \u2005 {1}".format(u"\U0001F476", cat), value=data[cat], inline=False)

    await cpp_student.send_message(ctx.message.author, embed=embed)

@my.command(pass_context=True)
async def date(ctx):
    embed = discord.Embed(
        title="{0: <25} \u2003 \u2003 \u2005 {1: <40} \u2003 \u2003 \u2003 \u2003 \u2002 \u2005 {2: <50}".format(
            "My Important Dates", "Activity", "Details"),
        description="", color=0xff8040)
    embed.set_author(
        name=ctx.message.author.name + "  |  Lorem ipsum dolor sit amet | consectetur adipiscing elit, sed do eiusmod",
        icon_url=ctx.message.author.avatar_url)
    embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))

    data = {}
    for item in inst_date_list:
        for month in item:
            record = ""
            for date, value in item[month].items():

                for acty, details in value.items():
                    record = record + "`" + "{0:} : {1: <18} {2: <20} {3: <23}".format(u"\U0001F4CC", date, acty,
                                                                                       details) + "`" + "\n"
            data[month] = record

    for month in data:
        embed.add_field(name="{0:} : {1}".format(u"\U0001F4C5", month), value=data[month], inline=False)

    await cpp_student.send_message(ctx.message.author, embed=embed)



@cpp_student.group(pass_context=True)
async def cohort(ctx):
    if ctx.invoked_subcommand is None or ctx.invoked_subcommand.name not in cmd_list:

        await cpp_student.say(
            "<@{}>, Invalid option..Supported options are ( {} )".format(ctx.message.author.id, ' | '.join(cmd_list)))

@cohort.command(pass_context=True)
async def feb18(ctx):
    embed = discord.Embed(title="",
                          description="", color=0xff8040)
    embed.set_author(
        name=ctx.message.author.name + "  |  Lorem ipsum dolor sit amet | consectetur adipiscing elit, sed do eiusmod",
        icon_url=ctx.message.author.avatar_url)
    embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))

    record = ""
    for course in student_list:
        for k, v in course.items():
            record = record + "`" + "{0:} : {1: <20} {2: <25}".format(u'\U0001F476', k, v) + "`" + "\n"


    embed.add_field(name= "{0: <20} \u2005 \u2005 \u2005 \u2005 \u2005 \u2005  {1: <40}".format("February-2018 Cohort", "Qualification"), value=record, inline=False)

    await cpp_student.send_message(ctx.message.author ,embed=embed)



@cpp_student.command(pass_context=True)
async def me(ctx, cmd: str = 'nill', subcmd: str = 'NILL'):
    this_cmd = cmd.lower()
    this_subcmd = subcmd.upper()
    this_user = ctx.message.author.id

    if this_cmd == 'nill' or this_cmd not in cmd_list:
        await cpp_student.say(
            "<@{}>, Invalid option..Supported options are ( {} )".format(this_user, ' | '.join(cmd_list)))

    elif this_cmd == 'grade':
        embed = discord.Embed(title="Summary of my Grades",
                              description="", color=0xff8040)
        embed.set_author(
            name=ctx.message.author.name + "  |  Student ID 91027986 | Diploma in Systems Technology Level 5",
            icon_url=ctx.message.author.avatar_url)
        embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))

        # data = {}
        # for course in grade_list:
        #     for k in course:
        #         record = ""
        #         for i, v in course[k].items():
        #             record = record + "`" + "{0: <18} {1: 10,.2f}% ".format(i, float(v)) + "`" + "\n"
        #         data[k] = record

        data = {}
        for course in grade_list:
            for firstKey in course:
                record = ""
                counter = 0
                for index, (secondKey, value) in enumerate(course[firstKey].items()):
                    if counter == 0:
                        record = record + "`"
                    record = record + "{0: <18} {1: 10,.2f}%    ".format(secondKey, float(value))
                    counter = counter + 1

                    if counter == 2:
                        record = record + "`"  + "\n"
                        counter = 0
                    elif index == len(course[firstKey]) - 1:
                        record = record + "`" + "\n"
                        counter = 0

                data[firstKey] = record


        for cat in data:
            embed.add_field(name="{0:} : {1}".format(u"\U0001F4D6", cat), value=data[cat], inline=False)

        await cpp_student.send_message(ctx.message.author,embed=embed)


    elif this_cmd == 'qual':
        embed = discord.Embed(title="",
                              description="", color=0xff8040)
        embed.set_author(
            name=ctx.message.author.name + "  |  Student ID 91027986 | Diploma in Systems Technology Level 5",
            icon_url=ctx.message.author.avatar_url)
        embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))

        record = ""
        for course in period_list:
            for k, v in course.items():
                record = record + "`" + "{0:} : {1: <25} {2: <30}".format(u'\U0001F4D2', k, v) + "`" + "\n"


        embed.add_field(name= "{0: <25} \u2003 \u2003 \u2003 \u2003 {1: <40} \u2003 \u2003 \u2002 {2: <50}".format("Summary of my Courses", "Start Date", "End Date"), value=record, inline=False)

        await cpp_student.send_message(ctx.message.author ,embed=embed)

    elif this_cmd == 'sum':
        embed = discord.Embed(title="My Important Dates",
                              description="", color=0xff8040)
        embed.set_author(name= ctx.message.author.name + "  |  Student ID 91027986 | Diploma in Systems Technology Level 5", icon_url=ctx.message.author.avatar_url)
        embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))



        data = {}
        for course in date_list:
            for firstKey in course:
                record = ""
                counter = 0
                for index, (secondKey, value) in enumerate(course[firstKey].items()):
                    if counter == 0:
                        record = record + "`"
                    record = record + "{0: <14} {1:<17}    ".format(secondKey, value)
                    counter = counter + 1

                    if counter == 2:
                        record = record + "`"  + "\n"
                        counter = 0
                    elif index == len(course[firstKey]) - 1:
                        record = record + "`" + "\n"
                        counter = 0

                data[firstKey] = record


        for cat in data:
            embed.add_field(name="{0:} : {1}".format(u"\U0001F4D6", cat), value=data[cat], inline=False)

        await cpp_student.send_message(ctx.message.author, embed=embed)

    elif this_cmd == 'review':
        if not str(ctx.message.channel).startswith("Direct Message with"):
            await cpp_student.say("`" + "Please proceed to your direct messaging channel with the bot." + "`")

        while True:
            await cpp_student.send_message(ctx.message.author, "`" + 'Press "g" to start the quiz' + "`")
            input = await cpp_student.wait_for_message(author=ctx.message.author)


            if input.clean_content.upper() == "G":
                count = 0
                point = 0
                for question in question_list:
                    for k in question:
                        response = ""
                        count = count + 1
                        for i, v in question[k].items():
                            if response == "":
                                await cpp_student.send_message(ctx.message.author, "`"  + "\u2753  " + i + " " + str(count) +"  :  "+ v + "`" )
                                response = await cpp_student.wait_for_message(author=ctx.message.author)
                            else:

                                if response.clean_content.upper() == i:
                                    await cpp_student.send_message(ctx.message.author, "`"+ "\U0001F609 " + "You are correct! \n" + v + "`" + "\n\n \u200b")
                                    response = ""
                                    point = point + 1

                                    while True:
                                        await cpp_student.send_message(ctx.message.author, "`" + "Press c to continue..." + "`")
                                        enter = await cpp_student.wait_for_message(author=ctx.message.author)
                                        if enter.clean_content.upper() == "C":
                                            break
                                        else:
                                            continue
                                else:
                                    await cpp_student.send_message(ctx.message.author,"`"+ "\U0001F61C " + 'You are wrong!\n' + v + "`" + "\n\n \u200b")
                                    response = ""

                                    while True:
                                        await cpp_student.send_message(ctx.message.author, "`" + "Press c to continue..." + "`")
                                        enter = await cpp_student.wait_for_message(author=ctx.message.author)
                                        if enter.clean_content.upper() == "C":
                                            break
                                        else:
                                            continue
                score = float(point)/float(count) * 100
                if score > 50:
                    remark = "You passed! \U0001F389"
                else:
                    remark = "You failed! \U0001F648 Please do more revisions."
                await cpp_student.send_message(ctx.message.author, "`" + "Quiz has ended.\n" + "Your score: {0: .2f}%\n".format(score)  + remark +"`")
                break
            else:
                continue

    elif this_cmd == 'stud':
        embed = discord.Embed(title="",
                              description="", color=0xff8040)
        embed.set_author(
            name=ctx.message.author.name + "  |  Lorem ipsum dolor sit amet | consectetur adipiscing elit, sed do eiusmod",
            icon_url=ctx.message.author.avatar_url)
        embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))

        record = ""
        for course in student_list:
            for k, v in course.items():
                record = record + "`" + "{0:} : {1: <20} {2: <25}".format(u'\U0001F476', k, v) + "`" + "\n"


        embed.add_field(name= "{0: <20} \u2003 \u2003 \u2005 \u2005 \u2005 \u2005 {1: <40}".format("Assigned Students", "Qualification"), value=record, inline=False)

        await cpp_student.send_message(ctx.message.author ,embed=embed)

    elif this_cmd == 'date':
        embed = discord.Embed(title="{0: <25} \u2003 \u2003 \u2005 {1: <40} \u2003 \u2003 \u2003 \u2003 \u2002 \u2005 {2: <50}".format("My Important Dates", "Activity", "Details"),
                              description="", color=0xff8040)
        embed.set_author(
            name=ctx.message.author.name + "  |  Lorem ipsum dolor sit amet | consectetur adipiscing elit, sed do eiusmod",
            icon_url=ctx.message.author.avatar_url)
        embed.set_footer(text="Delivered By " + cpp_student.user.name + " the " + str(datetime.now()))

        data = {}
        for item in inst_date_list:
            for month in item:
                record = ""
                for date, value in item[month].items():

                    for acty, details in value.items():

                        record = record + "`" + "{0:} : {1: <18} {2: <20} {3: <23}".format(u"\U0001F4CC", date, acty, details) + "`" + "\n"
                data[month] = record

        for month in data:
            embed.add_field(name="{0:} : {1}".format(u"\U0001F4C5", month), value=data[month], inline=False)

        await cpp_student.send_message(ctx.message.author ,embed=embed)



    else:
        await cpp_student.say(
            "<@{}>, hmmmmm, this command is unavailable for !my".format(this_user))


cpp_student.run(cfg.studentbot_token)
