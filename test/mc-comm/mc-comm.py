import discord
from discord.ext import commands
import logging
import config as cfg
import asyncio
import time
from datetime import datetime
import unicodedata
import api
from calendar import month_name

logging.basicConfig(filename='errors.log', level=logging.ERROR)

# create bot
mc_community = commands.Bot(command_prefix=cfg.mcbot_prefix)

# load permissions : return a list of discord user id
sec_list = api.get_permissions_list()
# load supported coin symbol : return a list of dict of coin id and coin symbol
coin_list = api.get_coins_list()
# supported parameters for !my command
my_cmd_list = ('bal', 'addresses', 'a', 'reward', 'r', 'deposits', 'd')
stake_cmd_list = ('stakes')

# image list
my_coin_img = {
    "1337": "https://i.imgur.com/yDjMHHf.png",
    "OK": "https://i.imgur.com/wvBvwcC.png",
    "BITB": "https://i.imgur.com/yAZ71Cy.png",
    "RDD": "https://i.imgur.com/xUKq3re.png",
    "SEND": "https://i.imgur.com/gAhDjBt.png",
    "EMB": "https://i.imgur.com/MFathT5.png",
    "INFO": "https://i.imgur.com/2a8JhCy.png",
    "INSN": "https://i.imgur.com/hUjHwDt.png",
    "STRAT": "https://i.imgur.com/EWWPcP0.png",
    "RADS": "https://i.imgur.com/sIUdAIM.png"
}


@mc_community.event
async def on_ready():
    print('---------')
    print('Logging in as')
    print(mc_community.user.name)
    print(mc_community.user.id)
    print("MC-Community bot online")
    print('---------')


@mc_community.command(pass_context=True)
async def sos(ctx):
    this_user = ctx.message.author.id
    check_permissions = True if this_user in sec_list else False

    if not check_permissions:
        yield from mc_community.say("<@{}>, {}".format(this_user, " Access Denied. Game Over man, Game Over"))
    else:
        this_msg = "```"
        this_msg = this_msg + "MC-COMMUNITY BOT COMMANDS LIST" + "\n\n"
        this_msg = this_msg + "{0: <40} : {1:}".format("!my bal | !my bal [coin]",
                                                       "return your personal total or per coin balances") + "\n"
        this_msg = this_msg + "{0: <40} : {1:}".format("!my deposits [coin] | !my d [coin]",
                                                       "return your last 10 deposits") + "\n"
        this_msg = this_msg + "{0: <40} : {1:}".format("!my addresses | !my a", "return your wallet addresses") + "\n"
        this_msg = this_msg + "{0: <40} : {1:}".format("!my reward | !my r",
                                                       "balances|rewards and targets for the month") + "\n"
        this_msg = this_msg + "{0: <40} : {1:}".format("!mc balance [coin] | !mc b [coin]",
                                                       "return community balances") + "\n"
        this_msg = this_msg + "{0: <40} : {1:}".format("!mc deposits [coin] | !mc d [coin]",
                                                       "return community members last 10 deposits") + "\n"
        this_msg = this_msg + "{0: <40} : {1:}".format("!mc addresses | !mc a", "return MC wallet addresses") + "\n"
        this_msg = this_msg + "{0: <40} : {1:}".format("!stakes", "return current month stakes for each coin") + "\n"
        this_msg = this_msg + "{0: <40} : {1:}".format("!rank [coin]", "return member ranking for a coin") + "\n"
        this_msg = this_msg + "```"
        yield from mc_community.say(this_msg)


@mc_community.group(pass_context=True)
async def my(ctx):
    this_user = ctx.message.author.id
    check_permissions = True if this_user in sec_list else False

    if not check_permissions:
        await mc_community.say("<@{}>, {}".format(this_user, " Access Denied. Game Over man, Game Over"))

    elif ctx.invoked_subcommand is None or ctx.invoked_subcommand.name not in my_cmd_list:
        await mc_community.say(
            "<@{}>, Invalid option..Supported options are ( {} )".format(this_user, ' | '.join(my_cmd_list)))


@mc_community.group(pass_context=True)
async def bal(ctx):

    if ctx.invoked_subcommand is None and 
    elif ctx.invoked_subcommand is None:
        embed = discord.Embed(title="{} portfolio balances".format(ctx.message.author.name),
                                  description="`" + "Summary of your wallet balances" + "`", color=0xff8040)
            embed.set_footer(text="Delivered By " + mc_community.user.name + " the " + str(datetime.now()))
            data = api.get_my_balance(this_user)
            tier = {}

            for coin in data:
                open_total = coin.total - coin.variation

                try:
                    variation = (coin.variation / open_total) * 100
                except ZeroDivisionError:
                    variation = 100

                variation = '+' + '{0: .2f}'.format(variation) + '%'

                if not tier.get(coin.tier, ""):
                    tier[coin.tier] = "`" + "{0:} : {1: <12} | {2: 15,.3f} | T{3:} | {4: >9}".format(
                        u'\U0001F4B0',
                        coin.name.upper(),
                        coin.total,
                        coin.tier, variation
                    ) + "`" + "\n"
                else:
                    tier[coin.tier] = tier[
                                          coin.tier] + "`" + "{0:} : {1: <12} | {2: 15,.3f} | T{3:} | {4: >9}".format(
                        u'\U0001F4B0',
                        coin.name.upper(),
                        coin.total, coin.tier, variation
                    ) + "`" + "\n"

            for level in sorted(tier):
                embed.add_field(name="Tier {}".format(level), value=tier[level], inline=False)

            yield from mc_community.say(embed=embed)

    elif this_cmd == 'balance' or this_cmd == 'b':
        if this_subcmd == 'NILL':
            

        elif any(d['coin_symbol'] == this_subcmd for d in coin_list):
            data_total_balance = api.get_my_total_coin_balance(this_user, this_subcmd)
            total_inc = 0
            if data_total_balance[0][3] == 0:
                yield from mc_community.say("<@{}>, Your {} balance is 0. No further data available".format(this_user,
                                                                                                            data_total_balance[
                                                                                                                0][1]))
            else:
                embed = discord.Embed(
                    title="{2} {1} Balance : +{0: ,.3f} {1}".format(data_total_balance[0][3],
                                                                             data_total_balance[0][2],ctx.message.author.name),
                    description="", color=0xff8040)
                # embed.set_author(name="Monte Crypto")
                # embed.set_thumbnail(url=my_coin_img[this_subcmd])
                embed.set_footer(text="Request Delivered By " + mc_community.user.name + " the " + str(datetime.now()))
                data = api.get_my_coin_balance(this_user, data_total_balance[0][0])

                for coin in data:
                    total_inc = total_inc + coin[4]
                    month_inc = coin[4] if (total_inc - coin[4]) == 0 else total_inc - coin[4]

                    this_coin = this_coin + "`" + "{0: <2}-{1: <4} : {2: 14,.3f} + | {3: 14,.3f} | +{4: 7,.2f}% | {5: >7} TX".format(
                        coin[2],
                        coin[3],
                        coin[4],
                        total_inc,
                        (coin[4] * 100) / month_inc,
                        coin[
                            5]) + "`" + "\n"

                embed.add_field(name="Last 12 Months Breakdown", value=this_coin, inline=False)
                yield from mc_community.say(embed=embed)
        else:
            yield from mc_community.say("<@{}>, Invalid coin Symbol, or not an active community coin".format(this_user))
    elif this_cmd == 'addresses' or this_cmd == 'a':
        # retrieve list of member wallet addresses
        data = api.get_addresses(this_user)
        embed = discord.Embed(title="{} community wallet addresses".format(ctx.message.author.name),
                              description="`" + "your wallet addresses" + "`", color=0xff8040)
        embed.set_footer(text="Delivered By " + mc_community.user.name + " the " + str(datetime.now()))
        for coin in data:
            this_coin = this_coin + "`" + "{0:} : {1: <13} | {2: >45}".format(u'\U0001F4D2', coin.name.upper(),
                                                                              coin.address) + "`" + "\n"
        embed.add_field(name="\u200b", value=this_coin, inline=False)
        yield from mc_community.say(embed=embed)

    elif this_cmd == 'deposits' or this_cmd == 'd':
        if subcmd == 'NILL':
            data = api.get_my_deposits(this_user)
            this_description = "Your latest contributions"
            embed = discord.Embed(title=ctx.message.author.name + " : 10 Last Deposits",
                                  description="`" + this_description + "`",
                                  color=0xff8040)
            embed.set_footer(text="Delivered By " + mc_community.user.name + " the " + str(datetime.now()))

            tx = ""
            for coin in data:
                tx = tx + "`" + "{0:} : {1: <13} {2: 15,.3f} {3: <6} | T{4:} | {5:%d-%b-%y} @ {5:%H:%M}".format(
                    u'\U0001F4BF',
                    cfg.mcbot_members[str(
                        this_user)],
                    coin.amount, coin.symbol.upper(),
                    coin.tier,
                    datetime.strptime(coin.received_at, '%Y-%m-%d %H:%M:%S.%f')) + "`" + "\n"

            embed.add_field(name="\u200b", value=tx, inline=False)
            yield from mc_community.say(embed=embed)

        elif any(d['coin_symbol'] == this_subcmd for d in coin_list):
            data = api.get_my_deposits_by_coin(this_user, this_subcmd)

            if data:
                this_description = "Your latest contributions for {}".format(data[0].name)
                embed = discord.Embed(title=ctx.message.author.name + " : 10 Last Deposits",
                                      description="`" + this_description + "`",
                                      color=0xff8040)
                embed.set_footer(text="Delivered By " + mc_community.user.name + " the " + str(datetime.now()))

                tx = ""
                for coin in data:
                    tx = tx + "`" + "{0:} : {1: <13} {2: 15,.3f} {3: <6} | T{4:} | {5:%d-%b-%y} @ {5:%H:%M}".format(
                        u'\U0001F4BF',
                        cfg.mcbot_members[
                            str(
                                this_user)],
                        coin.amount,
                        coin.symbol.upper(),
                        coin.tier,
                        datetime.strptime(
                            coin.received_at,
                            '%Y-%m-%d %H:%M:%S.%f')) + "`" + "\n"

                embed.add_field(name="\u200b", value=tx, inline=False)
                yield from mc_community.say(embed=embed)
            else:
                yield from mc_community.say("Your balance is 0 {}, no transactions found.".format(this_subcmd))
        else:
            yield from mc_community.say("<@{}>, Invalid coin Symbol, or not an active community coin".format(this_user))

    elif this_cmd == 'reward' or this_cmd == 'r':
        data = api.get_my_reward(this_user)
        this_reward = "{}-{} Balance|Target|Reward Summary ".format(month_name[datetime.now().month],
                                                                    datetime.now().year) + "\n"
        this_reward = "```" + this_reward + "\n" + "{0: <12} : {1: >16} | {2: >13} | {3: >14} | {4: >13} | {5: >13}".format(
            "Coin",
            "Balance",
            "Target",
            "Achieved",
            "My Reward", "Reward max") + "\n"
        for coin in data:
            achieved = 100 if ((coin.User_Total_Balance * 100) / coin.Balance_Target) >= 100 else (
                (coin.User_Total_Balance * 100) / coin.Balance_Target)
            try:
                rewarded = ((achieved * coin.Monthly_Stakes) / 100) if achieved > 0 else 0
            except:
                rewarded = 0
            this_reward = this_reward + "{0: <12} : {1: 16,.3f} | {2: 13,.3f} | {3: 13,.3f}% | {4: 13,.3f} | {5: 13,.3f}".format(
                coin.name, coin.User_Total_Balance, coin.Balance_Target, achieved, rewarded,
                coin.Monthly_Stakes) + "\n"

        this_reward = this_reward + "```"
        yield from mc_community.say(this_reward)

    else:
        yield from mc_community.say(
            "<@{}>, hmmmmm, this command is unavailable for !my".format(this_user))


@mc_community.command(pass_context=True)
@asyncio.coroutine
def mc(ctx, cmd: str = 'nill', subcmd: str = 'NILL'):
    this_coin = ""
    this_cmd = cmd.lower()
    this_subcmd = subcmd.upper()
    this_user = ctx.message.author.id
    check_permissions = True if this_user in sec_list else False

    if not check_permissions:
        yield from mc_community.say("<@{}>, {}".format(this_user, " Access Denied. Game Over man, Game Over"))

    elif this_cmd == 'nill' or this_cmd not in my_cmd_list:
        yield from mc_community.say(
            "<@{}>, Invalid command, please use ( {} ) after !my".format(this_user, ' | '.join(my_cmd_list)))

    elif this_cmd == 'balance' or this_cmd == 'b':
        if this_subcmd == 'NILL':
            embed = discord.Embed(title="Monte Crypto portfolio balances",
                                  description="`" + "Combined balances of all members" + "`", color=0xff8040)
            data = api.get_mc_balance()
            tier = {}
            price_btc = api.get_btc_prices()
            total_btc = 0

            for coin in data:
                open_total = coin.total - coin.variation

                try:
                    variation = (coin.variation / open_total) * 100
                except ZeroDivisionError:
                    variation = 100

                variation = '+' + "{0: .2f}".format(variation) + '%'
                total_btc = total_btc + (float(price_btc.get(coin.symbol, 0))*coin.total)

                if not tier.get(coin.tier, ""):
                    tier[coin.tier] = "`" + "{0:} : {1: <12} | {2: 15,.3f} | T{3:} | {4: >9} | {5: 11,.8f} BTC".format(
                        u'\U0001F4B0',
                        coin.name.upper(),
                        coin.total,
                        coin.tier, variation,float(price_btc.get(coin.symbol, 0))*coin.total
                    ) + "`" + "\n"
                else:
                    tier[coin.tier] = tier[
                                          coin.tier] + "`" + "{0:} : {1: <12} | {2: 15,.3f} | T{3:} | {4: >9} | {5: 11,.8f} BTC".format(
                        u'\U0001F4B0',
                        coin.name.upper(),
                        coin.total, coin.tier, variation,float(price_btc.get(coin.symbol, 0))*coin.total
                    ) + "`" + "\n"

            for level in sorted(tier):
                embed.add_field(name="Tier {}".format(level), value=tier[level], inline=False)

            embed.set_footer(text="Delivered By " + mc_community.user.name + " the " + str(datetime.now()) + " | " + "TOTAL BALANCE : {0: 2,.3f} BTC".format(total_btc))
            yield from mc_community.say(embed=embed)
        elif any(d['coin_symbol'] == this_subcmd for d in coin_list):
            data_total_balance = api.get_mc_total_coin_balance(this_subcmd)
            total_inc = 0
            if data_total_balance[0][3] == 0:
                yield from mc_community.say(
                    "{} balance is 0. No further data available".format("Monte Crypto", data_total_balance[0][1]))
            else:
                embed = discord.Embed(
                    title="Monte Crypto {1} Balance : +{0: ,.3f} {1}".format(data_total_balance[0][3], data_total_balance[0][2]),
                    description="", color=0xff8040)
                #embed.set_author(name="Monte Crypto")
                #embed.set_thumbnail(url=my_coin_img[this_subcmd])
                embed.set_footer(text="Request Delivered By " + mc_community.user.name + " the " + str(datetime.now()))
                data = api.get_mc_coin_balance(data_total_balance[0][0])

                for coin in data:
                    total_inc = total_inc + coin[4]
                    month_inc = coin[4] if (total_inc - coin[4]) == 0 else total_inc - coin[4]

                    this_coin = this_coin + "`" + "{0: <2}-{1: <4} : {2: 14,.3f} + | {3: 14,.3f} | +{4: 7,.2f}% | {5: >7} TX".format(
                        coin[2],
                        coin[3],
                        coin[4],
                        total_inc,
                        (coin[4] * 100) / month_inc,
                        coin[
                            5]) + "`" + "\n"

                embed.add_field(name="Last 12 Months Breakdown", value=this_coin, inline=False)
                yield from mc_community.say(embed=embed)
        else:
            yield from mc_community.say("<@{}>, Invalid coin Symbol, or not an active community coin".format(this_user))
    elif this_cmd == 'addresses' or this_cmd == 'a':

        data = api.get_addresses(mc_community.user.id)
        embed = discord.Embed(title="{} community wallet addresses".format(mc_community.user.name),
                              description="`" + "mc-community wallet addresses" + "`", color=0xff8040)
        embed.set_footer(text="Delivered By " + mc_community.user.name + " the " + str(datetime.now()))
        for coin in data:
            this_coin = this_coin + "`" + "{0:} : {1: <13} | {2: >45}".format(u'\U0001F4D2', coin.name.upper(),
                                                                              coin.address) + "`" + "\n"
        embed.add_field(name="\u200b", value=this_coin, inline=False)
        yield from mc_community.say(embed=embed)

    elif this_cmd == 'deposits' or this_cmd == 'd':
        if subcmd == 'NILL':
            data = api.get_mc_deposits()
            this_description = "Our members valuable latest contributions"
            embed = discord.Embed(title="Monte Crypto Community: 10 Last Deposits",
                                  description="`" + this_description + "`",
                                  color=0xff8040)
            embed.set_footer(text="Delivered By " + mc_community.user.name + " the " + str(datetime.now()))

            tx = ""
            for coin in data:
                tx = tx + "`" + "{0:} : {1: <13} {2: 15,.3f} {3: <6} | T{4:} | {5:%d-%b-%y} @ {5:%H:%M}".format(
                    u'\U0001F4BF',
                    cfg.mcbot_members[str(
                        coin.discord_id)],
                    coin.amount, coin.symbol.upper(),
                    coin.tier,
                    datetime.strptime(coin.received_at, '%Y-%m-%d %H:%M:%S.%f')) + "`" + "\n"

            embed.add_field(name="\u200b", value=tx, inline=False)
            yield from mc_community.say(embed=embed)

        elif any(d['coin_symbol'] == this_subcmd for d in coin_list):
            data = api.get_mc_deposits_by_coin(this_subcmd)

            if data:
                this_description = "Our members valuable latest contributions for {}".format(data[0].name)
                embed = discord.Embed(title="Monte Crypto Community: 10 Last Deposits",
                                      description="`" + this_description + "`",
                                      color=0xff8040)
                embed.set_footer(text="Delivered By " + mc_community.user.name + " the " + str(datetime.now()))

                tx = ""
                for coin in data:
                    tx = tx + "`" + "{0:} : {1: <13} {2: 15,.3f} {3: <6} | T{4:} | {5:%d-%b-%y} @ {5:%H:%M}".format(
                        u'\U0001F4BF',
                        cfg.mcbot_members[
                            str(
                                coin.discord_id)],
                        coin.amount,
                        coin.symbol.upper(),
                        coin.tier,
                        datetime.strptime(
                            coin.received_at,
                            '%Y-%m-%d %H:%M:%S.%f')) + "`" + "\n"

                embed.add_field(name="\u200b", value=tx, inline=False)
                yield from mc_community.say(embed=embed)
            else:
                yield from mc_community.say("Community balance is 0 {}, no transactions found.".format(this_subcmd))
    else:
        yield from mc_community.say(
            "<@{}>, hmmmmm, this command is unavailable for !mc".format(this_user))


@mc_community.command(pass_context=True)
@asyncio.coroutine
def stakes(ctx, cmd: str = 'nill'):
    this_coin = "`" + "{0: <12} : {1: >13} | {2: <12} | {3: <12} | {4: <12}".format("Coin", "Stakes", "Members 50%",
                                                                                    "Compound 25%",
                                                                                    "Purchase 25%") + "`" + "\n"
    this_coin = this_coin + "`" + "-------------------------------------------------------------------------" + "`" + "\n"
    this_user = ctx.message.author.id
    check_permissions = True if this_user in sec_list else False

    if not check_permissions:
        yield from mc_community.say("<@{}>, {}".format(this_user, " Access Denied. Game Over man, Game Over"))

    else:
        embed = discord.Embed(color=0xff8040)
        embed.set_author(name=mc_community.user.name, icon_url=mc_community.user.avatar_url)
        embed.set_footer(text="Request Delivered By " + mc_community.user.name + " the " + str(datetime.now()))
        data = api.get_month_stakes()

        for coin in data:
            this_coin = this_coin + "`" + "{0: <12} : {1: 13,.3f} | {2: 12,.3f} | {3: 12,.3f} | {4: 12,.3f}".format(
                coin[0].upper(), coin[2], float(coin[2] / 2), float(coin[2] / 4), float(coin[2] / 4)) + "`" + "\n"

        embed.add_field(name="{}-{} Staking Report".format(month_name[datetime.now().month], datetime.now().year),
                        value=this_coin,
                        inline=False)
        yield from mc_community.say(embed=embed)


@mc_community.command(pass_context=True)
@asyncio.coroutine
def rank(ctx, cmd: str = 'nill'):
    this_coin = ""
    this_cmd = cmd.upper()
    this_user = ctx.message.author.id
    counter = 1
    check_permissions = True if this_user in sec_list else False

    if not check_permissions:
        yield from mc_community.say("<@{}>, {}".format(this_user, " Access Denied. Game Over man, Game Over"))

    elif this_cmd == 'NILL' or not any(d['coin_symbol'] == this_cmd for d in coin_list):
        yield from mc_community.say(
            "<@{}>, Invalid coin symbol, please use a valid community coin symbol. {}".format(this_user, this_cmd))
    else:
        data = api.get_rank(this_cmd)
        this_title = "{} member balance ranking".format(data[0].name)
        embed = discord.Embed(title=this_title,
                              description="`" + "with your precious target line" + "`",
                              color=0xff8040)
        embed.set_footer(text="Request Delivered By " + mc_community.user.name + " the " + str(datetime.now()))

        sum_it = sum((total[3] for total in data))

        target = sum_it / 12
        chk = False

        for rank in data:
            percent = ((rank[3] * 100) / sum_it)
            bars = int(round((((percent * 20) / 100))))
            if counter == 1:
                medal = u"\U0001F947"
            elif counter == 2:
                medal = u"\U0001F948"
            elif counter == 3:
                medal = u"\U0001F949"
            else:
                if rank[3] > 0:
                    medal = u"\U0001F648"
                else:
                    medal = u"\U0001F983"
            if rank[3] > target:
                this_coin = this_coin + "`" + "{0:} : {1: <2} {2: <12} {3: 15,.3f} {4: <20} {5: 7,.2f}%".format(medal,
                                                                                                                counter,
                                                                                                                cfg.mcbot_members[
                                                                                                                    str(
                                                                                                                        rank[
                                                                                                                            2])],
                                                                                                                rank[3],
                                                                                                                u'\u25A0' * bars,
                                                                                                                percent) + "`" + "\n"
            else:
                if not chk:
                    this_coin = this_coin + "`" + "{0:}       {1: <12} {2: 15,.3f} {3: <20} {4: >8}".format(u"\u25B6",
                                                                                                            " ", float(
                            target), " ", "target") + "`" + "\n"
                    this_coin = this_coin + "`" + "{0:} : {1: <2} {2: <12} {3: 15,.3f} {4: <20} {5: 7,.2f}%".format(
                        medal, counter, cfg.mcbot_members[str(rank[2])], rank[3], u'\u25A0' * bars,
                        percent) + "`" + "\n"
                    chk = True
                else:
                    this_coin = this_coin + "`" + "{0:} : {1: <2} {2: <12} {3: 15,.3f} {4: <20} {5: 7,.2f}%".format(
                        medal, counter, cfg.mcbot_members[str(rank[2])], rank[3], u'\u25A0' * bars,
                        percent) + "`" + "\n"
            counter += 1

        embed.add_field(name="\u200b", value=this_coin, inline=False)
        yield from mc_community.say(embed=embed)


mc_community.run(cfg.mcbot_token)
