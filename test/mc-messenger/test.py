import simplejson
import model
import time
from datetime import datetime,timedelta

engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=False)
session_maker = model.sessionmaker(bind=engine)
session = session_maker()


def get_last_transactions(now):
    last_entries = session.query(model.Deposit).filter(model.Deposit.received_at >= now).order_by(model.Deposit.date.desc())

    results = []
    if last_entries:
        for item in last_entries:
            results.append(
                {
                    "Date": time.strftime("%d-%b-%y @ %H:%M", time.localtime(item.date)),
                    "Member": item.this_wallet.discord_id,
                    "Amount": item.amount,
                    "Coin": item.this_wallet.this_coin.symbol,
                    "Type": item.amount_type,
                    "Received" : "{0:%d-%b-%y} @ {0:%H:%M}".format(datetime.strptime(item.received_at,'%Y-%m-%d %H:%M:%S.%f'))


                }
            )

    return results

#last_check = (time.mktime(datetime.now().timetuple())) - 20000
delta_now = datetime.now() - timedelta(hours=24)
results = get_last_transactions(delta_now)

for i in results:
    print(i)