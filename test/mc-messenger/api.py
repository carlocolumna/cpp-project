import simplejson
import time
from datetime import datetime, timedelta
import model
from sqlalchemy import extract

# create DB mapping
engine = model.create_engine('sqlite:///C:\\Users\\kaLi\\Documents\\CPP-Project\\test\\db\\db.db', echo=True)
#engine = model.create_engine('sqlite:///C:\\Users\\carlo\\Documents\\cpp-project\\test\\db\\db.db', echo=True)

# create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

date = datetime.now()
date_last_hour = (date - timedelta(hours=0))





def get_last_transactions(model, session, date_last_hour):
    current_month = datetime.now().strftime('%m')
    last_hour = date_last_hour.strftime('%H')

    last_deposit_entries = session.query(model.Deposit).filter(extract('month', model.Deposit.date) == current_month).all()
    last_stake_entries = session.query(model.Stake).filter(extract('month', model.Stake.date) == current_month).all()


    results = []
    if last_deposit_entries:
        for item in last_deposit_entries:
            item_hour = datetime.strptime(item.date, '%Y-%m-%d %H:%M').strftime('%H')
            if item_hour == last_hour:
                results.append(
                    {
                        "Date": "{0:%d-%b-%y} @ {0:%H:%M}".format(datetime.strptime(item.date,'%Y-%m-%d %H:%M')),
                        "Member": item.this_wallet.discord_id,
                        "Amount": item.close_bal - item.open_bal_daily,
                        "Coin": item.this_wallet.this_coin.symbol,
                        "Tier": item.this_wallet.this_coin.tier,
                        "Type": True
                    }
                )

    if last_stake_entries:
        for item in last_stake_entries:
            item_hour = datetime.strptime(item.date, '%Y-%m-%d %H:%M').strftime('%H')
            print(last_hour)
            print(item_hour)
            if item_hour == last_hour:
                results.append(
                    {
                        "Date": "{0:%d-%b-%y} @ {0:%H:%M}".format(datetime.strptime(item.date,'%Y-%m-%d %H:%M')),
                        "Member": "391817760826458115",
                        "Amount": item.close_stake - item.open_stake_daily,
                        "Coin": item.this_coin.symbol,
                        "Tier": item.this_coin.tier,
                        "Type": False
                    }
                )

    return results

data = get_last_transactions(model, session, date_last_hour)

print(data)
for item in data:
    print(item)