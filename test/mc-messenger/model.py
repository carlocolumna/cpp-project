from sqlalchemy import create_engine, Column, Integer, String, Boolean, Float, ForeignKey ,exc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker,relationship

Base = declarative_base()

engine = create_engine('sqlite:///C:\\Users\\kaLi\\Documents\\CPP-Project\\test\\db\\db.db', echo=True)


class Coin(Base):
    __tablename__ = "coin"
    id = Column('id', Integer, primary_key=True)
    symbol = Column('symbol', String(10), unique=True)
    name = Column('name', String(25), unique=True)
    rpc_port = Column('rpc_port', Integer, unique=True)
    version = Column('version', String(50))
    tier = Column('tier', Integer)
    status = Column ('status', Integer)
    online = Column('online', Boolean, default = False)
    price = Column('price', Float)
    wallets = relationship('Wallet' , backref='this_coin', lazy='dynamic')
    stakes = relationship('Stake', backref='this_coin', lazy='dynamic')

class Wallet(Base):
    __tablename__ = "wallet"
    id = Column('id', Integer, primary_key=True)
    wallet_type = Column('wallet_type', Integer, default=1)
    address = Column('address', String(255), unique=True)
    discord_id = Column('discord_id', String(25), unique=False)
    coin_id = Column(Integer, ForeignKey('coin.id'))
    deposits = relationship('Deposit', backref='this_wallet', lazy='dynamic')


class Deposit(Base):
    __tablename__ = "deposit"
    id = Column ('id', Integer, primary_key=True)
    open_bal_monthly = Column('open_bal_monthly', Float)
    open_bal_daily = Column('open_bal_daily', Float)
    close_bal = Column('close_bal', Float)
    date = Column('date', Integer)
    wallet_id = Column(Integer, ForeignKey('wallet.id'))

class Stake(Base):
    __tablename__ = "stake"
    id = Column ('id', Integer, primary_key=True)
    open_stake_monthly = Column('open_stake_monthly', Float)
    open_stake_daily = Column('open_stake_daily', Float)
    close_stake = Column('close_stake', Float)
    date = Column('date', Integer)
    coin_id = Column(Integer, ForeignKey('coin.id'))


Base.metadata.create_all(bind=engine)
