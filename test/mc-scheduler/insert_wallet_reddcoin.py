import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_rdd = model.Wallet()
indiana_rdd.wallet_type = False
indiana_rdd.address = "RnUAkwn6qUDpNxzYeGJtkLDtE3sdSsd5tn"
indiana_rdd.discord_id = "264272631560142848"
indiana_rdd.coin_id = 5

pixel_rdd = model.Wallet()
pixel_rdd.wallet_type = False
pixel_rdd.address = "RgVbFzJXagsXeL2FgfcweBxkPprTHhoaXy"
pixel_rdd.discord_id = "157024659546701824"
pixel_rdd.coin_id = 5

kali_rdd = model.Wallet()
kali_rdd.wallet_type = False
kali_rdd.address = "Rq5zXTBGz3Ge2U5tUYkiL97oeVo3dxaezL"
kali_rdd.discord_id = "273550198691856394"
kali_rdd.coin_id = 5

mc_rdd = model.Wallet()
mc_rdd.wallet_type = True
mc_rdd.address = "RktnqF7tw8fggwfx6p44b1VNjMgB14sFtJ"
mc_rdd.discord_id = "388984890076692481"
mc_rdd.coin_id = 5

badwolf_rdd = model.Wallet()
badwolf_rdd.wallet_type = False
badwolf_rdd.address = "RnqudSxiF8eio2evUKtPGSSMtRMvzFbxay"
badwolf_rdd.discord_id = "311809553626824714"
badwolf_rdd.coin_id = 5

babyai_rdd = model.Wallet()
babyai_rdd.wallet_type = False
babyai_rdd.address = "RiKwHCGGLwoUg9gKyYWiSaCivAuLGW4exx"
babyai_rdd.discord_id = "311058941150756864"
babyai_rdd.coin_id = 5

riio_rdd = model.Wallet()
riio_rdd.wallet_type = False
riio_rdd.address = "RrDnf2p1R8FaPzccPCHNUExiahRqd3ZQDG"
riio_rdd.discord_id = "210913059231760384"
riio_rdd.coin_id = 5

session.add(indiana_rdd)
session.add(pixel_rdd)
session.add(kali_rdd)
session.add(mc_rdd)
session.add(badwolf_rdd)
session.add(babyai_rdd)
session.add(riio_rdd)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()