import model
import logging
from bitcoinrpc.authproxy import AuthServiceProxy
import time
from datetime import datetime

logging.basicConfig(filename='errors.log', level=logging.ERROR)

engine = model.create_engine('sqlite:///C:\\Users\\kaLi\\Documents\\CPP-Project\\test\\db\\db.db', echo=True)

#engine = create_engine('mssql+pyodbc://montecrypto:Vz5i?Ey-QS1A@montecrypto', echo=True)
# model.Base.metadata.create_all(bind=engine)

# create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()


def settings():
    user = 'JaWs'
    password = 'pf114834'
    conn = []
    account_addresses = []
    coins = session.query(model.Coin).filter(model.Coin.online == True).all()
    addresses = session.query(model.Wallet).all()

    for coin in coins:
        for address in addresses:
            if coin.id == address.coin_id:
                account_addresses.append(
                    {
                        "wallet_id": address.id,
                        "discord_id": address.discord_id,
                        "wallet_address": address.address,
                        "wallet_type": address.wallet_type
                    }
                )
        thiscoin = coin.__dict__
        thiscoin["addresses"] = account_addresses
        thiscoin["rpc"] = AuthServiceProxy('http://{0}:{1}@127.0.0.1:{2}'.format(user, password, coin.rpc_port))
        thiscoin["user"] = user
        thiscoin["password"] = password
        conn.append(coin.__dict__)
        account_addresses = []
    return conn


data = settings()
global_counter = 0
# insert_counter = 0




print("\nProgram is running. Started at {}\n".format(datetime.now()))

while True:
    # setting current time
    now = datetime.now()
    # start header display
    print("=============================")
    print("Connecting to {} Blockchain".format(data[global_counter]["symbol"]))
    print("=============================")
    print("")
    print("Starting tick... @ {}".format(now))

    total_current_balance = data[global_counter]["rpc"].getbalance()
    print(data[global_counter]["symbol"])

    total_deposits_from_addresses = 0

    for wallet in data[global_counter]["addresses"]:
        if wallet['discord_id'] != '391817760826458115': #MC-Messenger discord id used to identify MC-Stakes address
            latest_total_address_deposits = data[global_counter]["rpc"].getreceivedbyaddress(wallet['wallet_address'])  # Total deposits of one address coming (blockchain)
            total_deposits_from_addresses = total_deposits_from_addresses + latest_total_address_deposits               # Total deposits from all addresses in a desktop wallet

            #print(wallet['wallet_address'] + " : " + str(latest_total_address_deposits))

            # Update the Deposit table in database
            deposit_record = session.query(model.Deposit).filter(model.Deposit.wallet_id == wallet['wallet_id']).order_by(model.Deposit.date.desc()).first()       # Deposit record from the database

            if deposit_record:

                latest_deposit_record_month = datetime.strptime(deposit_record.date, '%Y-%m-%d %H:%M').strftime('%m')
                #deposit_record_month = time.strftime("%m", deposit_record.date.timetuple())
                #current_month = time.strftime("%m", time.localtime())
                current_month = now.strftime('%m')


                if latest_deposit_record_month == current_month:

                    # Check if the recorded total deposits of an address is less than the latest total deposits.
                    # If it is, it means there's a new deposit to the address so update the database.
                    if deposit_record.close_bal < latest_total_address_deposits:

                        # Gets the current day and compare it to the record's date. If not equal,
                        # update the opening balance for the day
                        current_day = now.strftime('%d')
                        latest_stake_record_day = datetime.strptime(deposit_record.date, '%Y-%m-%d %H:%M').strftime('%d')
                        if latest_stake_record_day != current_day:
                            deposit_record.open_bal_daily = deposit_record.close_bal

                        # Update the closing balance and the date on the latest deposit details
                        deposit_record.close_bal = latest_total_address_deposits
                        deposit_record.date = now.strftime('%Y-%m-%d %H:%M')
                        session.commit()

                    elif deposit_record.close_bal  == float(latest_total_address_deposits):                                            # If equal, no changes to be made; converting to float removes trailing zeroes
                        print("Skipping: Latest " + wallet['wallet_address'] + " deposit and database record are equal")
                    else:
                        print(deposit_record.close_bal)
                        print(latest_total_address_deposits)
                        print("Something's off on the DEPOSIT records")

                else:
                    deposit = model.Deposit(open_bal_monthly=deposit_record.close_bal, open_bal_daily=deposit_record.close_bal, close_bal=latest_total_address_deposits,
                                            date=now.strftime('%Y-%m-%d %H:%M'), wallet_id=wallet['wallet_id'])
                    session.add(deposit)
                    session.commit()
                    print("Inserted the new deposit record at start of the month")

            else:
                deposit = model.Deposit(open_bal_monthly = latest_total_address_deposits, open_bal_daily = latest_total_address_deposits, close_bal = latest_total_address_deposits,
                                        date = now.strftime('%Y-%m-%d %H:%M'), wallet_id = wallet['wallet_id'])
                session.add(deposit)
                session.commit()
                print("Inserted the first deposit record")

    # Update the Stake table in database
    total_stakes = total_current_balance - total_deposits_from_addresses

    stake_record = session.query(model.Stake).filter(model.Stake.coin_id == data[global_counter]["id"]).order_by(model.Stake.date.desc()).first()

    if stake_record:
        latest_stake_record_month = datetime.strptime(stake_record.date, '%Y-%m-%d %H:%M').strftime('%m')
        current_month = now.strftime('%m')

        if latest_stake_record_month == current_month:

            if stake_record.close_stake < total_stakes:
                stake_record.close_stake = total_stakes
                stake_record.date = now.strftime('%Y-%m-%d %H:%M')
                session.commit()

            elif stake_record.close_stake == float(total_stakes):                               # If equal, no changes to be made; converting to float removes trailing zeroes
                print("Skipping: Recorded total stake and latest total stake are equal")
            else:
                print("Something's off on the STAKE records")

        else:
            stake = model.Stake(open_stake_monthly=stake_record.close_stake, open_stake_daily = stake_record.close_stake, close_stake=total_stakes,
                                    date=now.strftime('%Y-%m-%d %H:%M'), coin_id=data[global_counter]["id"])
            session.add(stake)
            session.commit()
            print("Inserted the new stake record at start of the month")

    else:
        stake = model.Stake(open_stake_monthly=total_stakes, open_stake_daily=total_stakes, close_stake=total_stakes,
                            date=now.strftime('%Y-%m-%d %H:%M'), coin_id=data[global_counter]["id"])
        session.add(stake)
        session.commit()
        print("Inserted the first ever stake record")




    if global_counter >= len(data) - 1:
        global_counter = 0
    else:
        global_counter += 1

    # print("")
    # print("Ending tick...{} transaction(s) added to database".format(insert_counter))
    # print("")
    # insert_counter = 0

    time.sleep(5)




# global_counter = 0
# insert_counter = 0

# print("\nProgram is running. Started at {}\n".format(datetime.now()))

# while True:
#     # setting current time
#     now = datetime.now()
#     # start header display
#     print("=============================")
#     print("Connecting to {} Blockchain".format(data[global_counter]["symbol"]))
#     print("=============================")
#     print("")
#     print("Starting tick... @ {}".format(now))

#     list_tx = data[global_counter]["rpc"].listtransactions("*", 25, 0)

#     for tx in list_tx:
#         for wallet in data[global_counter]["addresses"]:
#             if wallet["wallet_address"] == tx.get("address", "n/a"):
#                 chksum = tx["txid"] + "-" + str(wallet["wallet_id"])
#                 result_chk = session.query(model.Deposit).filter(model.Deposit.chksum == chksum).first()

#                 if not result_chk:
#                     if tx["category"] == 'generate' or tx["category"] == 'stake' or tx["category"] == 'receive':
#                         mc_tx = model.Deposit()
#                         mc_tx.wallet_id = wallet["wallet_id"]
#                         mc_tx.txid = tx["txid"]
#                         mc_tx.chksum = tx["txid"] + "-" + str(wallet["wallet_id"])
#                         if tx["category"] == 'generate' or tx["category"] == 'stake':
#                             mc_tx.amount_type = True
#                         else:
#                             mc_tx.amount_type = False
#                         mc_tx.amount = tx["amount"]
#                         mc_tx.received_at = datetime.now()
#                         mc_tx.date = tx["time"]
#                         mc_tx.this_month = time.strftime("%m", time.localtime(tx["time"]))
#                         mc_tx.this_year = time.strftime("%Y", time.localtime(tx["time"]))
#                         session.add(mc_tx)
#                         session.commit()
#                         insert_counter += 1
#                         print("inserting {} {} from {}".format(tx["amount"], data[global_counter]["symbol"],
#                                                                tx["account"]))
#                     else:
#                         print("skipping")
#                     break
#                 else:
#                     if data[global_counter]["symbol"] == 'SEND':
#                         if tx["category"] == 'send' and int(tx['confirmations']) > 0 and int(
#                                 tx['bcconfirmations']) > 0 and float(tx["amount"]) < 0 and result_chk.amount_type == False:
#                             result_chk.amount = 6.79998960
#                             result_chk.amount_type = True
#                             session.commit()
#                             insert_counter += 1
#                             print("updating...{} record".format(insert_counter))
#                         else:
#                             pass
#                     else:
#                         pass
#             else:
#                 continue

#     if global_counter >= len(data) - 1:
#         global_counter = 0
#     else:
#         global_counter += 1

#     print("")
#     print("Ending tick...{} transaction(s) added to database".format(insert_counter))
#     print("")
#     insert_counter = 0

#     time.sleep(5)
