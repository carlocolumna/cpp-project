import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

#engine = model.create_engine('mssql+pyodbc://montecrypto:Vz5i?Ey-QS1A@montecrypto', echo=True)
engine = model.create_engine('sqlite:///C:\\Users\\kaLi\\Documents\\CPP-Project\\test\\db\\db.db', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_rbl = model.Wallet()
indiana_rbl.wallet_type = 1
indiana_rbl.address = "rEXedSdQRzVCwrLkDmtraDDNdqHxmvPifu"
indiana_rbl.discord_id = "264272631560142848"
indiana_rbl.coin_id = 13

pixel_rbl = model.Wallet()
pixel_rbl.wallet_type = 1
pixel_rbl.address = "rEfR9Xih1QmbBk7vjq39XmUAxvj6fi2V26"
pixel_rbl.discord_id = "157024659546701824"
pixel_rbl.coin_id= 13

kali_rbl = model.Wallet()
kali_rbl.wallet_type = 1
kali_rbl.address = "rRqqZHYpNFVFG1K8HMhoVqJk8iqCpDhi3i"
kali_rbl.discord_id = "273550198691856394"
kali_rbl.coin_id= 13

mc_rbl = model.Wallet()
mc_rbl.wallet_type = 2
mc_rbl.address = "rE4dm6HuP8sWPWfhXSjZq6tqB8tJYoAHoh"
mc_rbl.discord_id = "388984890076692481"
mc_rbl.coin_id= 13

badwolf_rbl = model.Wallet()
badwolf_rbl.wallet_type = 1
badwolf_rbl.address = "rFyJFLnnGQBTuX85jhagMrLHDraJAK7D6z"
badwolf_rbl.discord_id = "311809553626824714"
badwolf_rbl.coin_id= 13

sparkling_rbl = model.Wallet()
sparkling_rbl.wallet_type = 3
sparkling_rbl.address = "rHvvucj1swxdhotowSEpjDg6rMw4Pt7ySD"
sparkling_rbl.discord_id = "311058941150756864"
sparkling_rbl.coin_id= 13

riio_rbl = model.Wallet()
riio_rbl.wallet_type = 1
riio_rbl.address = "rNuiz4dQUEQAXRQiLFHTthWPPfkHPjNuKE"
riio_rbl.discord_id = "210913059231760384"
riio_rbl.coin_id= 13

mc_stake_rbl = model.Wallet()
mc_stake_rbl.wallet_type = 4
mc_stake_rbl.address = "rBM8oA68TFTbHEZSAoNjLuDPuyXaXR5HR6"
#use id of mc-messenger.
mc_stake_rbl.discord_id = "391817760826458115"
mc_stake_rbl.coin_id= 13

session.add(indiana_rbl)
session.add(pixel_rbl)
session.add(kali_rbl)
session.add(mc_rbl)
session.add(badwolf_rbl)
session.add(sparkling_rbl)
session.add(riio_rbl)
session.add(mc_stake_rbl)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()