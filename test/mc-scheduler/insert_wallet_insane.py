import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

engine = model.create_engine('sqlite:///C:\\bots\\db\\mc-db.db', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_insn = model.Wallet()
indiana_insn.wallet_type = False
indiana_insn.address = "iEvLuJzp4gGfvRT18tmn67tGKiCAmBZhcn"
indiana_insn.discord_id = "264272631560142848"
indiana_insn.coin_id = 8

pixel_insn = model.Wallet()
pixel_insn.wallet_type = False
pixel_insn.address = "iAvTDzxc8oHZPBJEaV8aygTZSUf9LhepaK"
pixel_insn.discord_id = "157024659546701824"
pixel_insn.coin_id = 8

kali_insn = model.Wallet()
kali_insn.wallet_type = False
kali_insn.address = "iEY3ZtiAh1SAmZCA2MEULiMDMZnufLHMc6"
kali_insn.discord_id = "273550198691856394"
kali_insn.coin_id = 8

mc_insn = model.Wallet()
mc_insn.wallet_type = True
mc_insn.address = "iQ9tgJKoQkta1uyETxY61Vn7M7sgwPV14q"
mc_insn.discord_id = "388984890076692481"
mc_insn.coin_id = 8

badwolf_insn = model.Wallet()
badwolf_insn.wallet_type = False
badwolf_insn.address = "i47A38A8uUWr1wmZYpAqYzcLc9yn3pXZY8"
badwolf_insn.discord_id = "311809553626824714"
badwolf_insn.coin_id = 8

babyai_insn = model.Wallet()
babyai_insn.wallet_type = False
babyai_insn.address = "iFkdaUDXGeguURFQhVKuHYGgucgbvN9Be3"
babyai_insn.discord_id = "311058941150756864"
babyai_insn.coin_id = 8

riio_insn = model.Wallet()
riio_insn.wallet_type = False
riio_insn.address = "i7Cy9VSkWaJZMMNbFoxR86V9XxH2SwfvRq"
riio_insn.discord_id = "210913059231760384"
riio_insn.coin_id = 8

session.add(indiana_insn)
session.add(pixel_insn)
session.add(kali_insn)
session.add(mc_insn)
session.add(badwolf_insn)
session.add(babyai_insn)
session.add(riio_insn)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()