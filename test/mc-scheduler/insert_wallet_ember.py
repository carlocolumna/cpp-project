import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

engine = create_engine('mssql+pyodbc://montecrypto:Vz5i?Ey-QS1A@montecrypto', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_ember = model.Wallet()
indiana_ember.wallet_type = False
indiana_ember.address = "e3Rc39qpdCinxF69x1CHcA6HDnPbqt3RoX"
indiana_ember.discord_id = "264272631560142848"
indiana_ember.opening_balance = 0
indiana_ember.current_balance = 0
indiana_ember.coin_id = 2

pixel_ember = model.Wallet()
pixel_ember.wallet_type = False
pixel_ember.address = "eKDP7XH6Y82tZQ4eP5ajFKDczwpQopVim7"
pixel_ember.discord_id = "157024659546701824"
pixel_ember.opening_balance = 0
pixel_ember.current_balance = 0
pixel_ember.coin_id = 2

kali_ember = model.Wallet()
kali_ember.wallet_type = False
kali_ember.address = "eBWTx86MvyivGbZsouLcQusD7hxZHBVerD"
kali_ember.discord_id = "273550198691856394"
kali_ember.opening_balance = 0
kali_ember.current_balance = 0
kali_ember.coin_id = 2

mc_ember = model.Wallet()
mc_ember.wallet_type = True
mc_ember.address = "eJ3ftZgAPGyk8rBCFBiAbCqASUbtXjgKX1"
mc_ember.discord_id = "388984890076692481"
mc_ember.opening_balance = 0
mc_ember.current_balance = 0
mc_ember.coin_id = 2

badwolf_ember = model.Wallet()
badwolf_ember.wallet_type = False
badwolf_ember.address = "eAg5cnHxVNr7Kq8kazymgUMH65zj3mQ7Mg"
badwolf_ember.discord_id = "311809553626824714"
badwolf_ember.opening_balance = 0
badwolf_ember.current_balance = 0
badwolf_ember.coin_id = 2

sparkling_ember = model.Wallet()
sparkling_ember.wallet_type = False
sparkling_ember.address = "eKgnHscQsPCCyjUm3SLTLdZb4wuVx5JYxY"
sparkling_ember.discord_id = "311058941150756864"
sparkling_ember.opening_balance = 0
sparkling_ember.current_balance = 0
sparkling_ember.coin_id = 2

riio_ember = model.Wallet()
riio_ember.wallet_type = False
riio_ember.address = "eRK3fFVPNnk2T6qHkfYRSaeLc6JGKa8r3x"
riio_ember.discord_id = "210913059231760384"
riio_ember.opening_balance = 0
riio_ember.current_balance = 0
riio_ember.coin_id = 2

mc_stake_ember = model.Wallet()
mc_stake_ember.wallet_type = False
mc_stake_ember.address = "1"
#use id of mc-messenger.
mc_stake_ember.discord_id = "391817760826458115"
mc_stake_ember.opening_balance = 0
mc_stake_ember.current_balance = 0
mc_stake_ember.coin_id = 2

session.add(indiana_ember)
session.add(pixel_ember)
session.add(kali_ember)
session.add(mc_ember)
session.add(badwolf_ember)
session.add(sparkling_ember)
session.add(riio_ember)
session.add(mc_stake_ember)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()