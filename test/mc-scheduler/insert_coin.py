import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)
engine = model.create_engine('sqlite:///C:\\Users\\kaLi\\Documents\\CPP-Project\\test\\db\\db.db', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

leet = model.Coin()
leet.symbol = "1337"
leet.name = "1337"
leet.rpc_port = 13372
leet.tier = 6
leet.status = "unknown"
leet.version = "N/A"

emb = model.Coin()
emb.symbol = "EMB"
emb.name = "Ember"
emb.rpc_port = 10022
emb.tier = 6
emb.status = "unknown"
emb.version = "N/A"

okcash = model.Coin()
okcash.symbol = "OK"
okcash.name = "OKCash"
okcash.rpc_port = 6969
okcash.tier = 4
okcash.status = "unknown"
okcash.version = "N/A"

bitbean = model.Coin()
bitbean.symbol = "BITB"
bitbean.name = "Bitbean"
bitbean.rpc_port = 22461
bitbean.tier = 5
bitbean.status = "unknown"
bitbean.version = "N/A"

reddcoin = model.Coin()
reddcoin.symbol = "RDD"
reddcoin.name = "Reddcoin"
reddcoin.rpc_port = 45443
reddcoin.tier = 5
reddcoin.status = "unknown"
reddcoin.version = "N/A"

send = model.Coin()
send.symbol = "SEND"
send.name = "Social Send"
send.rpc_port = 50050
send.tier = 6
send.status = "unknown"
send.version = "N/A"

insane = model.Coin()
insane.symbol = "INSN"
insane.name = "INSaNe"
insane.rpc_port = 4867
insane.tier = 6
insane.status = "unknown"
insane.version = "N/A"

radium = model.Coin()
radium.symbol = "RADS"
radium.name = "Radium"
radium.rpc_port = 27914
radium.tier = 3
radium.status = "unknown"
radium.version = "N/A"

stratis = model.Coin()
stratis.symbol = "STRAT"
stratis.name = "Stratis"
stratis.rpc_port = 8332
stratis.tier = 2
stratis.status = "unknown"
stratis.version = "N/A"

info = model.Coin()
info.symbol = "INFO"
info.name = "Infocoin"
info.rpc_port = 42857
info.tier = 7
info.online = True
info.status = "unknown"
info.version = "N/A"

nomad = model.Coin()
nomad.symbol = "NMD"
nomad.name = "Nomad"
nomad.rpc_port = 15717
nomad.tier = 7
nomad.online = True
nomad.status = "unknown"
nomad.version = "N/A"

katana = model.Coin()
katana.symbol = "KTX"
katana.name = "Katana"
katana.rpc_port = 31944
katana.tier = 7
katana.online = True
katana.status = "unknown"
katana.version = "N/A"

rainbow = model.Coin()
rainbow.symbol = "RBL"
rainbow.name = "RainbowLite"
rainbow.rpc_port = 14122
rainbow.tier = 7
rainbow.online = True
rainbow.status = "unknown"
rainbow.version = "N/A"

session.add(leet)
session.add(emb)
session.add(okcash)
session.add(bitbean)
session.add(reddcoin)
session.add(info)
session.add(send)
session.add(insane)
session.add(radium)
session.add(stratis)
session.add(nomad)
session.add(katana)
session.add(rainbow)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()