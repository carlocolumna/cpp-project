import model
import logging

logging.basicConfig(filename='errors.log', level=logging.DEBUG)

#engine = model.create_engine('mssql+pyodbc://montecrypto:Vz5i?Ey-QS1A@montecrypto', echo=True)
engine = model.create_engine('sqlite:///C:\\Users\\kaLi\\Documents\\CPP-Project\\test\\db\\db.db', echo=True)

#create the sessionmaker and then a session
sessionmaker = model.sessionmaker(bind=engine)
session = sessionmaker()

indiana_ktx = model.Wallet()
indiana_ktx.wallet_type = 1
indiana_ktx.address = "KGnZU6nXsjSxwXwC2hVAEEQfUpf9RijeBk"
indiana_ktx.discord_id = "264272631560142848"
indiana_ktx.coin_id = 12

pixel_ktx = model.Wallet()
pixel_ktx.wallet_type = 1
pixel_ktx.address = "KThUpHx1S9z3ikCv7L6P7ZhzXazBx7jVHF"
pixel_ktx.discord_id = "157024659546701824"
pixel_ktx.coin_id = 12

kali_ktx = model.Wallet()
kali_ktx.wallet_type = 1
kali_ktx.address = "KQWCWbijT449UTnXqEBi29so4aTSH3i2A3"
kali_ktx.discord_id = "273550198691856394"
kali_ktx.coin_id = 12

mc_ktx = model.Wallet()
mc_ktx.wallet_type = 2
mc_ktx.address = "KGeJk41mBoCktfmU4fkc9P1LKqo6TTrRJ5"
mc_ktx.discord_id = "388984890076692481"
mc_ktx.coin_id = 12

badwolf_ktx = model.Wallet()
badwolf_ktx.wallet_type = 1
badwolf_ktx.address = "KDmCNsfEbfwCtatK9BvVJiC5qkCbTA3G5t"
badwolf_ktx.discord_id = "311809553626824714"
badwolf_ktx.coin_id = 12

sparkling_ktx = model.Wallet()
sparkling_ktx.wallet_type = 3
sparkling_ktx.address = "KVBuxqwza5ixkWAMifsQhSKQchct3Ygbew"
sparkling_ktx.discord_id = "311058941150756864"
sparkling_ktx.coin_id = 12

riio_ktx = model.Wallet()
riio_ktx.wallet_type = 1
riio_ktx.address = "KH4CFFsjmzrpgN7VazBp4eZNxTP8bq2W5F"
riio_ktx.discord_id = "210913059231760384"
riio_ktx.coin_id = 12

mc_stake_ktx = model.Wallet()
mc_stake_ktx.wallet_type = 4
mc_stake_ktx.address = "KCVPoLXr4tn6KQfrwA3b3Wf4fypD75XU5p"
#use id of mc-messenger.
mc_stake_ktx.discord_id = "391817760826458115"
mc_stake_ktx.coin_id = 12

session.add(indiana_ktx)
session.add(pixel_ktx)
session.add(kali_ktx)
session.add(mc_ktx)
session.add(badwolf_ktx)
session.add(sparkling_ktx)
session.add(riio_ktx)
session.add(mc_stake_ktx)

try:
    session.commit()
except Exception as err:
    session.rollback()
    logging.error("Failed to insert data : {}".format(err))
finally:
    session.close()